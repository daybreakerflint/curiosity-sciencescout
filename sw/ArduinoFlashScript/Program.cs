﻿/*
A tool to program many arduinos at once.
*/
using System.Diagnostics;
using System.IO.Ports;

internal class Program
{
    private static object serialLock = new object();
    private static int Main(string[] args)
    {
        List<string> oldSerialPorts = new List<string>();

        Console.WriteLine("Arduino Flashtool for CURIOSITY");
        string SoftwarePath = Path.GetFullPath("../Arduino/HardwareTest");
        ProcessStartInfo startInfo = new ProcessStartInfo()
        {
            FileName = "arduino-cli",
            Arguments = $"compile --fqbn arduino:avr:nano:cpu=atmega328old {SoftwarePath}",
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            UseShellExecute = false
        };

        var p = Process.Start(startInfo);
        p.WaitForExit();
        if (p.HasExited)
        {
            var standardOutput = p.StandardOutput.ReadToEnd();
            var errorOutput = p.StandardError.ReadToEnd();
            if (p.ExitCode == 0)
            {
                Console.WriteLine("Compiling software successful");
                Console.WriteLine(standardOutput);
            }
            else
            {
                Console.WriteLine("Failed to compile software --> Exit code " + p.ExitCode);
                if (!string.IsNullOrEmpty(standardOutput))
                    Console.WriteLine(standardOutput);
                Console.WriteLine(errorOutput);
                return 1;
            }
        }

        List<Thread> threads = new List<Thread>();
        while (true)
        {
            var port = getNewSerialPort();
            if (!string.IsNullOrEmpty(port))
            {
                Thread t = new Thread(() =>
                {
                    programDevice(port);
                });
                t.Start();
                threads.Add(t);
            }

            Thread.Sleep(50);

            if (threads.Count > 0)
            {
                for (int ctr = threads.Count - 1; ctr >= 0; ctr--)
                {
                    if (!threads[ctr].IsAlive)
                    {
                        threads.RemoveAt(ctr);
                    }
                }
            }
        }
        void programDevice(string port)
        {
            Console.WriteLine(timestamp() + "Programming device " + port);
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = "arduino-cli",
                // arduino-cli upload -p /dev/ttyUSB0 --fqbn arduino:avr:nano:cpu=atmega328old /home/daybreakerflint/Documents/projects/curiosity-sciencescout/sw/Arduino/HardwareTest

                Arguments = $"upload -p {port} --fqbn arduino:avr:nano:cpu=atmega328old {SoftwarePath}",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };
            var flashing = Process.Start(startInfo);
            flashing.WaitForExit();
            if (flashing.HasExited)
            {
                var standardOutput = p.StandardOutput.ReadToEnd();
                var errorOutput = p.StandardError.ReadToEnd();
                if (p.ExitCode == 0)
                {
                    /*
                    uint serial = 0;
                    lock (serialLock)
                    {
                        SerialPort serialPort = new SerialPort(port);
                        serialPort.Open();
                        var input = serialPort.ReadLine();
                        if (input.Contains("Enter serial number:"))
                        {
                            serial = getNextSerial();
                            serialPort.WriteLine($"{serial}");
                            input = serialPort.ReadLine();
                            var serial_str = input.Replace("Device serial number:", "").Trim();
                            var temp_serial = uint.Parse(serial_str);

                            if ((temp_serial == serial) && (temp_serial != 0x0000) && (temp_serial != 0xffff))
                            {
                                Console.WriteLine(timestamp() + $"Device ID set to {serial}");
                            }
                            else
                            {
                                Console.WriteLine(timestamp() + $"Failed to set serial number ({temp_serial})");
                                returnSerial(serial);
                            }
                        }
                        else if (input.Contains("Device serial number:"))
                        {
                            var serial_str = input.Replace("Device serial number:", "").Trim();
                            serial = uint.Parse(serial_str);
                            Console.WriteLine(timestamp() + $"Device ID: {serial}");
                        }
                        serialPort.Close();
                    }
                    */
                    Console.WriteLine(timestamp() + $"Software loaded sucessful on {port}");
                }
                else
                {
                    Console.WriteLine(timestamp() + $"Failed to programm device on {port} --> Exit code " + p.ExitCode);
                    if (!string.IsNullOrEmpty(standardOutput))
                        Console.WriteLine(standardOutput);
                    Console.WriteLine(errorOutput);
                }
            }
        }

        string getNewSerialPort()
        {
            string ret = "";
            List<string> serialPorts = new List<string>(Directory.GetFiles("/dev/", "ttyUSB*"));

            if (oldSerialPorts.Count > 0)
            {
                for (int ctr = oldSerialPorts.Count - 1; ctr >= 0; ctr--)
                {
                    if (!serialPorts.Contains(oldSerialPorts[ctr]))
                    {
                        oldSerialPorts.RemoveAt(ctr);
                    }
                }
            }

            if (serialPorts.Count >= 1)
            {
                foreach (string port in serialPorts)
                {
                    if (!oldSerialPorts.Contains(port))
                    {
                        ret = port;
                        oldSerialPorts.Add(port);
                        Console.WriteLine(timestamp() + ret + " detected");
                        break;
                    }
                }
            }
            return ret;
        }


        uint getNextSerial()
        {
            uint ret = 0;

            if (File.Exists("last_serial.txt"))
            {
                ret = uint.Parse(File.ReadAllText("last_serial.txt"));
                ret++;
            }
            else
            {
                ret = 1;
            }
            File.WriteAllText("last_serial.txt", $"{ret}");
            return ret;
        }

        void returnSerial(uint serial)
        {
            var temp = uint.Parse(File.ReadAllText("last_serial.txt"));
            if (temp == serial)
            {
                serial--;
                File.WriteAllText("last_serial.txt", $"{serial}");
            }
        }

        string timestamp()
        {
            return DateTime.Now.ToString("[yyyyMMdd hh:mm:ss.fff] ");
        }
    }
}
