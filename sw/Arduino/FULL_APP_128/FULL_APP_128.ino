/************************************************************************************************
 *  CURIOSITY_BULA_FULL_APP_V2.00_128A.ino
 *
 *  Target Board: ScienceScout CURIOSITY V2.00
 *    Target MPU: ATmega328P (Arduino Nano)
 *       Created: 04.2022
 *        Author: Daniel Bossy v/o Rodeo | HB9EUB
 *      Revision: V2.00
 *  Copyright by: HB9EUB (source is free for noncommercial and education project.)
 *          Note: I assume no liability for errors, defects and malfunctions that occur during
 *                the application of the codes.
 *                This code is free and open for anybody to use at their own risk.
 *      Function: Program for Curiosity on BULA22
 ************************************************************************************************/ 

#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoPixel.h>


/************************************************************************************************
 *  development, maintenance and program version option switch definitions
 ************************************************************************************************/

// #define DEBUG_SERIAL_OUTPUT  1               // active for debug informations over serial port (Monitor)

/************************************************************************************************
 *  internal arduino and external system hardware config
 ************************************************************************************************/

#define HEARDBEAT_LED_OF_ARDUINO              LED_BUILTIN


/*** OLED config ********************************************************************************/

#define SCREEN_WIDTH       128 // OLED display width, in pixels
#define SCREEN_HEIGHT       64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
// The pins for I2C are defined by the Wire-library. 
// On an arduino UNO:       A4(SDA), A5(SCL)
// On an arduino MEGA 2560: 20(SDA), 21(SCL)
// On an arduino LEONARDO:   2(SDA),  3(SCL), ...

#define OLED_RESET           4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS    0x3C // See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32 but for curiosity its 0x3C 
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif


/************************************************************************************************
 *  system software config
 ************************************************************************************************/

/*** Developement *******************************************************************************/

#define DEBUG                                   0       // 0 = the debugging code is inactive       | 1 = debugging code is activated


/************************************************************************************************
 *  global definitions
 ************************************************************************************************/ 


/************************************************************************************************
 *  global constant
 ************************************************************************************************/ 

#define RUN                              0b00000001    // system is in run mode
#define PROG                             0b00000010    // program mode bit

#define PROGRAM_LED_SHOW                 0b00000100    // program led show bit
#define PROGRAM_NEW_FRAME_DATA           0b00001000    // new frame data available

#define MENUHIDDEN                       0b00000000    // oled menu is showing
#define MENUDISPLAY                      0b00000001    // oled menu is showing

//#define ACTIVE_LED_SHOW_BIT_MASK         0b00110000    // bit mask for LED show number bits on ui8_SystemMode register
//#define ACTIVE_TEXT_SHOW_BIT_MASK        0b01110000    // bit mask for LED show number bits on ui8_SystemMode register


/************************************************************************************************
 *  global variable
 ************************************************************************************************/ 

uint8_t ui8_SystemMode = RUN;
uint8_t ui8_SystemMenu = MENUDISPLAY;
uint8_t ui8_SingleEvents = 0;

/************************************************************************************************
 *  global variable without any reset initialising
 ************************************************************************************************/
 

/************************************************************************************************
 *  local function prototype
 ************************************************************************************************/ 


/************************************************************************************************
 *  Public global definitions, variables and classes for Curiosity Software V1.01
 ************************************************************************************************/

#define NOP __asm__ __volatile__ ("nop\n\t");     // delay 62.5ns on a 16MHz AtMega like Arduino

/************************************************************************************************
 *  Button handling
 * 
 *  Global definitions, variables and classes for handling the Buttons.
 ************************************************************************************************/

#define PIN_BUTTON_UP           14
#define PIN_BUTTON_DOWN         15
#define PIN_BUTTON_LEFT         16
#define PIN_BUTTON_RIGHT        17
#define PIN_BUTTON_ENTER         2

#define PREG_BUTTON_DDR         DDRC
#define PREG_BUTTON_PORT        PORTC
#define PREG_BUTTON_PIN         PINC

#define BIT_BUTTON_UP            0
#define BIT_BUTTON_DOWN          1
#define BIT_BUTTON_LEFT          2
#define BIT_BUTTON_RIGHT         3
#define BIT_BUTTON_ENTER         4

#define BUTTON_STATUS            0
#define BUTTON_CHANGED           1
#define BUTTON_PRESSED           2
#define BUTTON_RELEASED          3

#define BUTTON_DEBOUNCE_DELAY   20      // [ms]

volatile  uint8_t ui8_ButtonInputRegister;            // current button input result register
volatile  uint8_t ui8_OldButtonInputRegister;         // previous button input result register
volatile  uint8_t ui8_ButtonPressedRegister;          // button pressed result register
volatile  uint8_t ui8_ButtonChangeRegister;           // button state change result register
volatile  uint8_t ui8_ButtonReleaseRegister;          // button release result register
volatile uint32_t ui8ary_ButtonPressedPastTime[5];    // how long has the button been pressed? 
volatile uint32_t ui32_Past;                          // time of last debouncing 


/************************************************************************************************
 *  EEPROM handling
 * 
 *  Global definitions, variables and classes for handling the EEPROM.
 *  Including the EEPROM file system.
 ************************************************************************************************/

#define EXT_EEPROM_CASE_TYPE                  DIP     // DIP = through hole part                  | SMD = surface mounted device

#if (EXT_EEPROM_CASE_TYPE == DIP)
  #define EEPROM_I2C_ADDRESS                  0x50    // EEPROM I2C Address
#elif (EXT_EEPROM_CASE_TYPE == SMD)
  #define EEPROM_I2C_ADDRESS                  0x51    // EEPROM I2C Address
#endif

#define EXT_EEPROM_SIZE                       128     // 512 = 512kbit = 64kByte = 25LC512 | 128 = 128kbit = 16kByte = 25LC128

#if (EXT_EEPROM_SIZE == 512)                                                    // 64kByte EEPROM

  #define EXT_EEPROM_SYSTEM_ADDRESS_LOGO                                0x0100  // Address for system logo 128 * 43 = 688 Byte from 0x0100 to 0x3B0
  #define EXT_EEPROM_SYSTEM_ADDRESS_USER_IMAGE                          0x1000  // Begin of the first user image
  #define EXT_EEPROM_SYSTEM_ADDRESS_USER_TEXT                           0x1800  // Begin of user text
  #define EXT_EEPROM_SYSTEM_ADDRESS_USER_SHOW                           0x1A00  // Begin of user LED Show
  #define EXT_EEPROM_SYSTEM_ADDRESS_GAME_IMAGES                         0xCFFF  // Begin of game images 128 Byte * 96 = 12288 Byte from 0xCFFF to 0xFFFF
  #define MAX_NUMBER_OF_USER_TEXTS                                           8  // Max texts
  #define MAX_OF_SHOWS                                                       4  // Max LED shows
  #define MAX_OF_IMAGES                                                      2  // Max images

#elif (EXT_EEPROM_SIZE == 128)                                                  // 16kByte EEPROM

  #define EXT_EEPROM_SYSTEM_ADDRESS_LOGO                                0x0100  // Address for system logo 128 * 43 = 688 Byte from 0x0100 to 0x3B0
  #define EXT_EEPROM_SYSTEM_ADDRESS_USER_IMAGE                          0x1000  // Begin of the first user image
  #define EXT_EEPROM_SYSTEM_ADDRESS_USER_TEXT                           0x1400  // Begin of user text
  #define EXT_EEPROM_SYSTEM_ADDRESS_USER_SHOW                           0x1500  // Begin of user LED Show
  #define EXT_EEPROM_SYSTEM_ADDRESS_GAME_IMAGES                         0x1A80  // Begin of game images 128 Byte * 96 = 12288 Byte from 0xCFFF to 0xFFFF
  #define MAX_NUMBER_OF_USER_TEXTS                                           4  // Max texts
  #define MAX_OF_SHOWS                                                       2  // Max LED shows
  #define MAX_OF_IMAGES                                                      1  // Max images

#endif


/************************************************************************************************
 *  Setup procedure as follows:
 *  
 *  1. If external EEPROM case type = DIP then set EXT_EEPROM_CASE_TYPE 1.
 *  2. If external EEPROM case type = SO8 then set EXT_EEPROM_CASE_TYPE 2.
 ************************************************************************************************/

//#define EXT_EEPROM_SYSTEM_ADDRESS_OSCAL                               0x0000  // Address for oscal setup data
//#define EXT_EEPROM_SYSTEM_ADDRESS_VREFCAL                             0x0002  // Address for Vref setup data

#define USER_IMAGE_FILESIZE                                             1024  // for simplification, in case of curiosity, the step size of the user images is always fixed at 1024 
#define SPLASH_SCREEN_IMAGE                                                0   // System splash screen logo


/************************************************************************************************
 *  NeoPixels handling
 * 
 *  Global definitions, variables and classes to handle the NeoPixel.
 *  Warning: Set BRIGHTNESS to max 50 otherwise you have problem with eye protection and heat generation!!!
 ************************************************************************************************/

#define LED_STRIP_PIN   9                                                     // Which pin on the Arduino is connected to the NeoPixels?
#define COUNT_OF_LED    4                                                     // How many NeoPixels are attached to the Arduino?
#define BRIGHTNESS     50                                                     // Set BRIGHTNESS to max 50 otherwise you have problem with eye protection and heat generation!!!


Adafruit_NeoPixel NeoPixelStrip = Adafruit_NeoPixel(COUNT_OF_LED, LED_STRIP_PIN, NEO_GRBW + NEO_KHZ800);  // Declare our NeoPixel strip object

                                                                              // Argument 1 = Number of pixels in NeoPixel strip
                                                                              // Argument 2 = Arduino pin number (most are valid)
                                                                              // Argument 3 = Pixel type flags, add together as needed:
                                                                              
                                                                              //   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs) Default value for Curiosity used SK6812 !!!
                                                                              //   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
                                                                              //   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
                                                                              //   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
                                                                              //   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products) Default value for Curiosity used SK6812 !!!


/************************************************************************************************
 *  System Power handling
 * 
 *  Global definitions and variables for handling system voltage monitoring and display.
 ************************************************************************************************/

/************************************************************************************************
 *  Calibration procedure as follows:
 *  
 *  1. Measure VREF at PIN3 of the ARDUINO and enter the value under VBAT_REF.
 *  2. Measure without voltage the resistance of R9  and enter the value under R9 in ohms.
 *  3. Measure without voltage the resistance of R10  and enter the value under R10 in ohms.
 ************************************************************************************************/
  
#define VBAT_MEASUREMENT_ON          5
#define VBAT_MEASUREMENT_INPUT      A6
#define ADC_FINALY_RESOLUTION      256        // Finaly result resolution

#define VBAT_REF                     1.073    // Actual measured voltage value of the internal 1.1V reference
#define R9                       38900        // Resistance of R9 in ohm     
#define R10                       9950        // Resistance of R10 in ohm     
#define VOLTAGE_DIVIDER_RATIO        4.9      // (float) ((R9 + R10)/R10)   // (R9 + R10)/R10 => (29k + 10k)/10k ~> 4.9

//volatile uint8_t ui8_SystemPowerLimit;
volatile uint8_t ui8_SystemPowerLimit = 1;


/************************************************************************************************
 *  User Text handling
 * 
 *  Global definitions and variables for the user text.
 *  A animated user text on the display.
 ************************************************************************************************/

#define MAX_LENGTH_PER_TEXT                          60    // char
#define MAX_NUMBER_OF_ANIMATION                       4
#define MAX_SIZE_OF_FONT                              4

uint8_t ui8_ShowTextNumber = 0;


/************************************************************************************************
 *  User LED Show handling
 * 
 *  Global definitions and variables for the user text.
 *  A animated user show on the neopixel.
 ************************************************************************************************/

#define FRAMES                  0
#define SHOW_DELAY              1                                                             // two byte space!!! high byte first
#define FRAME_LED_COUNT         3     

// begin of LED show LED datas and frame delay
#define FRAME_DATA_OFFSET       4

#define COUNT_OF_LED            4
#define COUNT_OF_LED_COLORS     4

#define FRAME_DELAY_SIZE        2                                                            // two byte space!!!
#define FRAME_DATA_SIZE         ((COUNT_OF_LED * COUNT_OF_LED_COLORS) + FRAME_DELAY_SIZE)    // bytes for frame datas

#define MAX_FRAMES_PER_SHOW     32

#define LED_SHOW_BLOCK_SIZE     (MAX_FRAMES_PER_SHOW * FRAME_DATA_SIZE)                      // do not work!?
#define LED_SHOW_SIZE           (LED_SHOW_BLOCK_SIZE + FRAME_DATA_OFFSET)

// RGBW DEFS
#define RED_LED                 0
#define GREEN_LED               1
#define BLUE_LED                2
#define WHITE_LED               3

#define LED_RUN                          0b00000001    // system is in run mode
#define PROGRAM                          0b00000010    // program mode bit
#define PROGRAM_LED_SHOW                 0b00000100    // program led show bit
#define PROGRAM_NEW_FRAME_DATA           0b00001000    // new frame data available
#define ACTIVE_LED_SHOW_BIT_MASK         0b00110000    // bit mask for LED show number bits on ui8_SystemMode register

uint8_t ui8_EepromProgramData[4];
        
//        ui8_EepromProgramData[FRAME_LED_COUNT] = COUNT_OF_LED;

uint8_t ui8_LedShowFrameBuffer[2 + FRAME_DATA_SIZE];   // size of buffer ShowNumber + FrameNumber + FRAME_DATA_SIZE

uint8_t ui8_LEDSystemMode = 0;                         // LED sytem running mode
uint8_t ui8_FrameLoop = 0;                             // show aktiv frame

uint8_t ui8_AvailableLEDShow = 0;
uint8_t ui8_BackFromLEDShow = 0;


/************************************************************************************************
 *  User Image handling
 * 
 *  Global definitions and variables for the user text.
 *  A user image on the display.
 ************************************************************************************************/

uint8_t ui8_ShowImageNumber = 0;


/************************************************************************************************
 *  Story Game handling
 * 
 *  Global definitions and variables for the story game.
 *  A game in which you have to invent a story based on the pictures you roll.
 ************************************************************************************************/
 
#define NUMBEROFCUBES        8
#define NUMBEROFIMAGES      75


/************************************************************************************************
 *  Flashlight handling
 * 
 *  Flashlight with advanced features such as white, red and green colors with brightness adjustment.
 *  VariousFlashing mode in red as well as an SOS function in white. 
 ************************************************************************************************/

#define WHITE_LIGHT     0
#define RED_LIGHT       1
#define GREEN_LIGHT     2

#define OFF             0
#define ON              1

#define NORMAL_OFF      0
#define NORMAL_ON       1
#define FLASH           2
#define STROBE          3
#define SOS             4

uint8_t ui8_TorchMode = 0;

typedef struct{
  uint8_t ui8_Color;
  uint8_t ui8_PreviousColor; 
  uint8_t ui8_Brightness;
  uint8_t ui8_Mode;
  uint8_t ui8_State;
} Flashlight_t;

Flashlight_t tds_Flashlight;


/************************************************************************************************
 *  User text handling
 * 
 ************************************************************************************************/


/************************************************************************************************
 *  Flashlight Morse timing handling 
 *  
 *  https://de.wikipedia.org/wiki/Morsecode
 *  https://www.qsl.net/dk5ke/geschwindigkeitsmessung.html
 *  
 *  A dash is 3 dots in length. 
 *  The length of the break is: 1 dot between two sent symbols
 *                              3 dot between letters in a word as well 
 *                              7 dot between words
 *                              
 *  With the values below, the Morse speed is 8 Wpm. 
 ************************************************************************************************/

#define MORSE_DOT           150     // Duration of a dot in ms
#define MORSE_DASH          450     // Duration of a dash in ms
#define MORSE_SIGN_SPACE    150     // Duration of the pause between two single dot or dash in ms
#define MORSE_CHAR_SPACE    450     // Duration of the pause between two single char in ms
#define MORSE_WORD_SPACE   1050     // Duration of the pause between two words in ms


/************************************************************************************************
 *  Terminal Server handling
 * 
 *  Global definitions and variables for handling of the terminal server.
 ************************************************************************************************/

 char chr_TerminalServerInput;

/************************************************************************************************
 *  Task handling
 * 
 *  Global definitions, variables and classes for handling the Tasks.
 ************************************************************************************************/
 
#define NUMBER_OF_TASKS             6         // how many tasks are in the system
#define TASK_ISNT_IN_USE            0         // the task is currently not used
#define TASK_IS_USED                1         // the task is currently used

#define USER_BUTTON_TASK            0
#define TERMINAL_SERVER_TASK        1         // number and position of the task
#define FLASHLIGHT_TASK             2
#define SYSTEM_POWER_LIMIT_TASK     3
#define SYSTEM_VOLTAGE_ON_SCREEN    4
#define USER_TEXT_DISPLAY_TASK      5

uint8_t ui8_Heartbeat_State = 0;              // hold the state of the Heartbeat LED (built-in Arduino LED on pin 13)

// time intervals for the tasks

#define INTERVAL_OF_TASK_0           1000     // blink every 1 second
#define INTERVAL_OF_TASK_1            500     // check serial events every 500 ms
#define INTERVAL_OF_TASK_2             50     // flashlight update every 50 ms
#define INTERVAL_OF_TASK_3_300000  300000     // get system power limit every 5.00 min
#define INTERVAL_OF_TASK_3_150000  150000     // get system power limit every 2.50 min
#define INTERVAL_OF_TASK_3_75000    75000     // get system power limit every 1.25 min
#define INTERVAL_OF_TASK_4          10000     // get system voltage and print on screen every 10 sek update
#define INTERVAL_OF_TASK_5             10     // user text animation update every 10 ms


uint32_t ui32ary_PreviousTimeOfTask[NUMBER_OF_TASKS];       // previous time for the tasks depending upon time.
uint32_t ui32ary_IntervalTimeOfTask[NUMBER_OF_TASKS] = {INTERVAL_OF_TASK_0, INTERVAL_OF_TASK_1, INTERVAL_OF_TASK_2, INTERVAL_OF_TASK_3_300000, INTERVAL_OF_TASK_4, INTERVAL_OF_TASK_5}; // interval time for the tasks.
 uint8_t ui8ary_TaskState[NUMBER_OF_TASKS] = {TASK_IS_USED, TASK_IS_USED, TASK_IS_USED, TASK_IS_USED, TASK_ISNT_IN_USE, TASK_ISNT_IN_USE}; // all tasks are in use


//  Prototypes

void task_0_Curiosity_Heartbeat(void);
void task_1_TerminalServer(void);            
void task_2_Flashlight(void);
void task_3_SystemPowerLimit(void);
void task_4_ShowVBAT(void);
void task_5_ShowUserText(void);

void (*functptr[])() = { task_0_Curiosity_Heartbeat, task_1_TerminalServer, task_2_Flashlight, task_3_SystemPowerLimit, task_5_ShowUserText, task_4_ShowVBAT };   // array with three functions


/************************************************************************************************
 *    EOF global definitions, constants & variables
 ************************************************************************************************/ 


/************************************************************************************************
 *  Public global definitions for Curiosity Hardware V1.04
 ************************************************************************************************/

#define STD_BUTTON_PRESSED_TIME_TO_ENTER               100
#define STD_BUTTON_PRESSED_TIME_TO_RELEASE               0  

#define ENTER_BUTTON_PRESSED_TIME_TO_ENTER_MENU        400
#define ENTER_BUTTON_PRESSED_TIME_TO_RELEASE_MENU      100
#define ENTER_BUTTON_HOLDLONG                         0x80
#define LEFT_BUTTON_HOLDLONG                          0x40
#define RIGHT_BUTTON_HOLDLONG                         0x20

#define APP_SINGLE_CALL                               0x01
#define BLACKSCREEN_SINGLE_CALL                       0x02

uint8_t ui8_RunState = 0; 
uint8_t ui8_MenuPos = 1;
uint8_t ui8_Changed = 1;

#define TORCH_APP         0
#define TEXT_APP          1
#define LED_APP           2
#define IMAGE_APP         3
#define GAME_APP          4
//#define TERMINAL_SRV_APP  5

uint8_t ui8_SelectedSystemMode = TORCH_APP; // TORCH_APP default


void staticMenu(void)
{

  display.fillRect(0, 0, display.width(), 12, WHITE);
  display.setTextSize(1);
  display.setTextColor(BLACK, WHITE);
  display.setCursor(10, 2);
  display.println(F("CURIOSITY FUNCTION")); // "Curiosity Funktion"
  display.setTextColor(WHITE, BLACK);

  display.setCursor(10, 20);
  display.print(F("APP: "));

  switch(ui8_SelectedSystemMode)
  {
    case TORCH_APP : { display.println(F("TORCH APP")); } break; //
    case TEXT_APP : { display.println(F("TEXT APP")); } break; //
    case LED_APP : { display.println(F("LED APP")); } break; //
    case IMAGE_APP : { display.println(F("IMAGE APP")); } break; //
    case GAME_APP : { display.println(F("GAME APP")); } break; //
//    case TERMINAL_SRV_APP : { display.println(F("TERMINAL SRV.")); } break; //
  }

  display.setCursor(10, 30);
  display.print(F("TEXT Nr: "));
  display.println(ui8_ShowTextNumber);

  display.setCursor(10, 40);
  display.print(F("LED SHOW Nr: ")); display.println((ui8_LEDSystemMode & ACTIVE_LED_SHOW_BIT_MASK) >> 4); loadLedShow((ui8_LEDSystemMode & ACTIVE_LED_SHOW_BIT_MASK) >> 4);

  display.setCursor(10, 50);
  display.print(F("IMAGE NR: ")); display.println(ui8_ShowImageNumber);

  // pos menu cursor 
  display.setCursor(2, (ui8_MenuPos * 10) + 10);
  display.println(F(">"));

}


void menuCheck(void)
{
  if (getButtonState((1<<BIT_BUTTON_DOWN), BUTTON_PRESSED)  && ui8_MenuPos < 4) {ui8_MenuPos++; ui8_Changed = 0x01;}
  if (getButtonState((1<<BIT_BUTTON_UP), BUTTON_PRESSED) && ui8_MenuPos > 1) {ui8_MenuPos--; ui8_Changed = 0x01;}
  
  switch (ui8_MenuPos) 
  {
    case 1 : {
               if (getButtonState((1<<BIT_BUTTON_LEFT), BUTTON_PRESSED)  && ui8_SelectedSystemMode > 0) {ui8_SelectedSystemMode--; ui8_Changed = 0x01;}
               if (getButtonState((1<<BIT_BUTTON_RIGHT), BUTTON_PRESSED) && ui8_SelectedSystemMode < 4) {ui8_SelectedSystemMode++; ui8_Changed = 0x01;}
             } break;

    case 2 : {
               if (getButtonState((1<<BIT_BUTTON_LEFT), BUTTON_PRESSED)  && ui8_ShowTextNumber > 0) {ui8_ShowTextNumber--; ui8_Changed = 0x01;}
               if (getButtonState((1<<BIT_BUTTON_RIGHT), BUTTON_PRESSED) && ui8_ShowTextNumber < MAX_NUMBER_OF_USER_TEXTS-1) {ui8_ShowTextNumber++; ui8_Changed = 0x01;}
             } break;

    case 3 : {
               uint8_t __ui8_LedShowNumber = (ui8_LEDSystemMode & ACTIVE_LED_SHOW_BIT_MASK) >> 4;
               if (getButtonState((1<<BIT_BUTTON_LEFT), BUTTON_PRESSED)  && __ui8_LedShowNumber > 0) {__ui8_LedShowNumber--; ui8_Changed = 0x01;}
               if (getButtonState((1<<BIT_BUTTON_RIGHT), BUTTON_PRESSED) && __ui8_LedShowNumber < MAX_OF_SHOWS-1) {__ui8_LedShowNumber++; ui8_Changed = 0x01;}
               ui8_LEDSystemMode &= ~ACTIVE_LED_SHOW_BIT_MASK;                                             // clear LED show number bits on ui8_LEDSystemMode register
               ui8_LEDSystemMode |= (__ui8_LedShowNumber & 0x03) << 4;
               loadLedShow ((ui8_LEDSystemMode & ACTIVE_LED_SHOW_BIT_MASK) >> 4);
             } break;

    case 4 : {
               if (getButtonState((1<<BIT_BUTTON_LEFT), BUTTON_PRESSED)  && ui8_ShowImageNumber > 0) {ui8_ShowImageNumber--; ui8_Changed = 0x01;}
               if (getButtonState((1<<BIT_BUTTON_RIGHT), BUTTON_PRESSED) && ui8_ShowImageNumber < MAX_OF_IMAGES-1) {ui8_ShowImageNumber++; ui8_Changed = 0x01;}
             } break;
  }      
}


/************************************************************************************************
 *    BOF Main functions
 *
 *     Function: Main functions for initial system and the main loop
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
/************************************************************************************************
 *    void setup()
 *
 *     Function: Arduiono startup function => C = int main(void)
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void setup() {

  Serial.begin(9600);
  Wire.begin();                                         // Connect to I2C bus as master

//  wdt_disable();
//  uint8_t ui8_EnterButtonStatePressedTime = 0;


  initialButtons();                                     // INITIALIZE ButtonDebounce, object (REQUIRED)

  /*
  Serial.println("STARTUP !!! ");  
  Serial.print("ui8_SystemState: ");Serial.println(ui8_SystemState);
  */


  
/*  
  // Wakeup routine
  if (ui8_SystemState == WAKEUP_STARTUP)
  {
      Serial.println("WAKEUP_STARTUP: "); 
      Serial.print("ui8_EnterButtonStatePressedTime: ");Serial.println(ui8_EnterButtonStatePressedTime);  
      Serial.print("digitalRead(PIN_BUTTON_ENTER: ");Serial.println(digitalRead(PIN_BUTTON_ENTER));
      while (digitalRead(PIN_BUTTON_ENTER) == LOW)                // While enter button is low increase counter.
      {
        ui8_EnterButtonStatePressedTime++;     
        Serial.println("InLoop: "); 
        Serial.print("ui8_EnterButtonStatePressedTime: ");Serial.println(ui8_EnterButtonStatePressedTime);  
        Serial.print("digitalRead(PIN_BUTTON_ENTER: ");Serial.println(digitalRead(PIN_BUTTON_ENTER));
        delay(1000);
        if (ui8_EnterButtonStatePressedTime == 3) {break;}
      }
      
  }
   
  Serial.print("ui8_SystemState: ");Serial.println(ui8_SystemState);
*/


  ADCSRA |= (1<<ADEN);                                  // switch Analog to Digitalconverter ON
  initialVBATMeasurement();                             // INITIALIZE VBAT measurement
//  ui8_SystemPowerLimit = getSystemPowerLimit();         // Get the system power limit
  initialNeopixel();                                    // INITIALIZE NeoPixel, object (REQUIRED)
  initialFlashlight();                                  // INITIALIZE Flashlight (REQUIRED)
  initialTaskManager();                                 // INITIALIZE TaskManager (REQUIRED)
  pinMode(HEARDBEAT_LED_OF_ARDUINO, OUTPUT);            // INITIALIZE Curiosity heartbeat
  initialDisplay();                                     // INITIALIZE Display, object (REQUIRED)

  getAvailableLedShows();

}


/************************************************************************************************
 *    void loop()
 *
 *     Function: Arduiono main loop => C = while(1)
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void loop() {

  taskManager();

  if (scanButtons() != 0)
  {

 //   Serial.print(F("ui8_SystemMode: ")); Serial.print(ui8_SystemMode); Serial.print(F(" ui8_SystemMenu: ")); Serial.print(ui8_SystemMenu); Serial.print(F(" ui8_SelectedSystemMode: ")); Serial.println(ui8_SelectedSystemMode);

    if (((ui8_SystemMode & RUN) == RUN) && ((ui8_SystemMenu & MENUDISPLAY) == MENUHIDDEN))                      // RUN APP;
    {
      switch(ui8_SelectedSystemMode)                                                                            // which application is running
      {
        case TORCH_APP : {  } break;                                                                            // torch mode select
  
        case TEXT_APP :  {  } break;                                                                            // show user text on OLED
        
//        case LED_APP :   { if (getButtonState((1<<BIT_BUTTON_ENTER), BUTTON_PRESSED)) {setupFlashlight ( WHITE, BRIGHTNESS, NORMAL_OFF); ui8_TorchMode = 0; ui8_SystemMenu |= MENUDISPLAY; ui8_Changed = 0x01; clearLedStripe();}} break;
        case LED_APP :   {  } break;
        
        case IMAGE_APP : { 
                           if ((ui8_SingleEvents & APP_SINGLE_CALL) == 0)                                       // Call APP only one time 
                           { 
                             display.clearDisplay();                                                            // Make sure the display is cleared
                             drawImageFromEEPROM(ui8_ShowImageNumber);                                          // Display selected user image
                             display.display();
                             ui8_SingleEvents |= APP_SINGLE_CALL;
                           }
                         } break; //

        case GAME_APP : { 
                           if ((ui8_SingleEvents & APP_SINGLE_CALL) == 0)                                 // Call APP only one time on first 
                           {     
                             storyGameGamble();
                             ui8_SingleEvents |= APP_SINGLE_CALL;
                           }
                           else
                           {
                             if (getButtonState((1<<BIT_BUTTON_RIGHT), BUTTON_PRESSED)) {storyGameGamble();}  // Call APP for new gamble
                           }
                         } break; // new game
      }
    }
  }

  if (((ui8_SystemMode & RUN) == RUN) && ((ui8_SystemMenu & MENUDISPLAY) == MENUHIDDEN))                      // RUN APP;
  {
    switch(ui8_SelectedSystemMode)                                                                  // which application is running
    {
      case TORCH_APP : {                                                                                    // torch mode select
                 uint8_t ui8_OldTorchMode = ui8_TorchMode;
  
                 if (getButtonState((1<<BIT_BUTTON_RIGHT),BUTTON_STATUS))
                 {
                   uint32_t ui32_PastTime = getButtonPastTime(BIT_BUTTON_RIGHT);
                   uint32_t __ui32_CompareTime;
                   __ui32_CompareTime = STD_BUTTON_PRESSED_TIME_TO_ENTER;                     
                   if (ui32_PastTime > __ui32_CompareTime)
                   {
                     if ((ui8_SingleEvents & RIGHT_BUTTON_HOLDLONG) == 0)
                     {
//                         if (ui8_TorchMode < 3) ui8_TorchMode++;
                       if (ui8_TorchMode < 5) ui8_TorchMode++;
                       ui8_SingleEvents |= RIGHT_BUTTON_HOLDLONG; // changed (only one time running)
                     }
                   }
                 }
  
                 if (getButtonState((1<<BIT_BUTTON_LEFT),BUTTON_STATUS))
                 {
                   uint32_t ui32_PastTime = getButtonPastTime(BIT_BUTTON_LEFT);
                   uint32_t __ui32_CompareTime;
                   __ui32_CompareTime = STD_BUTTON_PRESSED_TIME_TO_ENTER;                     
                   if (ui32_PastTime > __ui32_CompareTime)
                   {
                     if ((ui8_SingleEvents & LEFT_BUTTON_HOLDLONG) == 0)
                     {
                       if (ui8_TorchMode > 0) ui8_TorchMode--;
                       ui8_SingleEvents |= LEFT_BUTTON_HOLDLONG; // changed (only one time running)
                     }
                   }
                 }
  
  
                 if (getButtonState((1<<BIT_BUTTON_LEFT),BUTTON_RELEASED) || getButtonState((1<<BIT_BUTTON_RIGHT),BUTTON_RELEASED))
                 {
                   ui8_SingleEvents &= ~(LEFT_BUTTON_HOLDLONG | RIGHT_BUTTON_HOLDLONG);
                 }
  
  
                 if (ui8_OldTorchMode != ui8_TorchMode)
                 {
                   switch (ui8_TorchMode)
                   {
                     case 0 : {setupFlashlight ( WHITE, BRIGHTNESS, NORMAL_OFF);} break;
                     case 1 : {switchFlashlight(OFF); setupFlashlight ( WHITE_LIGHT, BRIGHTNESS, NORMAL_ON);} break;
                     case 2 : {switchFlashlight(OFF); setupFlashlight ( RED_LIGHT, BRIGHTNESS, NORMAL_ON);} break;
                     case 3 : {switchFlashlight(OFF); setupFlashlight ( GREEN_LIGHT, BRIGHTNESS, NORMAL_ON);} break;
                    // case 4 : {setupFlashlight ( WHITE, BRIGHTNESS, FLASH);} break;
                    // case 5 : {setupFlashlight ( WHITE, BRIGHTNESS, STROBE);} break;
                    // case 6 : {setupFlashlight ( WHITE, BRIGHTNESS, SOS);} break;
                   }
                 }
               } break;

      case TEXT_APP : { scrollLongText(); } break;                                                         // show user text on OLED
      
      case LED_APP : { 
                       getNextLedShowFrame((ui8_LEDSystemMode & ACTIVE_LED_SHOW_BIT_MASK) >> 4);
                       if (getButtonState((1<<BIT_BUTTON_ENTER), BUTTON_PRESSED)) {setupFlashlight ( WHITE, BRIGHTNESS, NORMAL_OFF); ui8_TorchMode = 0; ui8_SystemMenu |= MENUDISPLAY; ui8_Changed = 0x01; clearLedStripe(); ui8_BackFromLEDShow = 0x01;}
                     } break; // show user LED animation
      
      case IMAGE_APP : {  } break; //
               
      case GAME_APP : { 
   //              ui8_SingleEvents &= ~APP_SINGLE_CALL;                            // Reset Call APP only one time on first
               } break; // new game 
    }
  }


/*
  if (scanButtons() != 0)
  {
    if (getButtonState((1<<BIT_BUTTON_ENTER),BUTTON_PRESSED))  Serial.println(F("ENTER PRESSED")); Serial.println();
    if (getButtonState((1<<BIT_BUTTON_ENTER),BUTTON_CHANGED))  Serial.println(F("ENTER CHANGED")); Serial.println();
    if (getButtonState((1<<BIT_BUTTON_ENTER),BUTTON_RELEASED)) Serial.println(F("ENTER RELEASED")); Serial.println();
  }
*/

  if (getButtonState((1<<BIT_BUTTON_ENTER),BUTTON_STATUS))
  {
    uint32_t ui32_PastTime = getButtonPastTime(BIT_BUTTON_ENTER);
//    Serial.print(F("ENTER PRESSED HOLD ")); Serial.print(ui32_PastTime); Serial.println(F(" ENTER PRESSED HOLD"));
    uint32_t __ui32_CompareTime;
    if ((ui8_SystemMenu & MENUDISPLAY) == MENUDISPLAY) {__ui32_CompareTime = ENTER_BUTTON_PRESSED_TIME_TO_RELEASE_MENU;} else {__ui32_CompareTime = ENTER_BUTTON_PRESSED_TIME_TO_ENTER_MENU;}
    if (ui32_PastTime > __ui32_CompareTime)
    {
//      Serial.println(F("ENTER EVENT"));
      if ((ui8_SystemMenu & ENTER_BUTTON_HOLDLONG) == 0)
      {
//        Serial.println(F("HOLDLONG ACTIVE"));
//        Serial.println(ui8_SystemMenu);
        if ( ui8_BackFromLEDShow == 0){
        if ((ui8_SystemMenu & MENUDISPLAY) == MENUDISPLAY) {ui8_SystemMenu &= ~MENUDISPLAY;} else { setupFlashlight ( WHITE, BRIGHTNESS, NORMAL_OFF); ui8_TorchMode = 0; ui8_SystemMenu |= MENUDISPLAY; ui8_Changed = 0x01; ui8_SingleEvents &= ~(ENTER_BUTTON_HOLDLONG | LEFT_BUTTON_HOLDLONG | RIGHT_BUTTON_HOLDLONG);}
        ui8_SystemMenu |= ENTER_BUTTON_HOLDLONG; // changed (only one time running)
        }
      }
    }
  }

  if (getButtonState((1<<BIT_BUTTON_ENTER),BUTTON_RELEASED))
  {
    ui8_SystemMenu &= ~ENTER_BUTTON_HOLDLONG;
    ui8_BackFromLEDShow = 0x00;
  }
 
  if ((ui8_SystemMenu & MENUDISPLAY) == MENUDISPLAY)
  {
//    ui8_SystemMode = PROG;
//    ui8_SingleEvents &= ~APP_SINGLE_CALL;                            // Reset Call APP only one time on first
    menuCheck();
    if (ui8_Changed)
    {
      display.clearDisplay(); 
      staticMenu();
      display.display();
      ui8_Changed = 0x00;
      ui8_SingleEvents &= ~(BLACKSCREEN_SINGLE_CALL | APP_SINGLE_CALL);                            // Reset Call blackscreen only one time on
//      Serial.print(F("IMAGE ui8_SingleEvents: ")); Serial.println(ui8_SingleEvents);
    }
  }
  else
  {
//    ui8_SystemMode = RUN;
    if ((ui8_SingleEvents & BLACKSCREEN_SINGLE_CALL) == 0)
    {
      display.clearDisplay(); 
      display.display();
      ui8_SingleEvents |= BLACKSCREEN_SINGLE_CALL;
    }
  }

  delay(50);
}


/************************************************************************************************
 *    EOF: main functions
 ************************************************************************************************/

/************************************************************************************************
 *    BOF System core functions
 *    
 *     Function: Main functions for initial system and the main loop
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

/************************************************************************************************
 *    void softReset(void)
 *
 *     Function: Restarts program from beginning but does not reset the peripherals and registers.
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
/*
void softReset(void) 
{
  asm volatile ("  jmp 0");  
}
*/

/************************************************************************************************
 *    void wakeUp(void)
 *
 *     Function: Restarts the program from the beginning, but only if Enter is 
 *               pressed longer than 3 seconds. Otherwise the system will go back to deep sleep. 
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
/*
void wakeUp(void)
{
  uint8_t ui8_EnterButtonStatePressedTime = 0;

    Serial.println("wakeUp enter: "); 

  Serial.print("ui8_EnterButtonStatePressedTime: ");Serial.println(ui8_EnterButtonStatePressedTime);  
  Serial.print("digitalRead(PIN_BUTTON_ENTER: ");Serial.println(digitalRead(PIN_BUTTON_ENTER));

  while (digitalRead(PIN_BUTTON_ENTER) == LOW)                // While enter button is low increase counter.
  {
    ui8_EnterButtonStatePressedTime++;     
  Serial.println("InLoop: "); 
  Serial.print("ui8_EnterButtonStatePressedTime: ");Serial.println(ui8_EnterButtonStatePressedTime);  
  Serial.print("digitalRead(PIN_BUTTON_ENTER: ");Serial.println(digitalRead(PIN_BUTTON_ENTER));
    delay(1000);
    if (ui8_EnterButtonStatePressedTime == 3) {break;}
  }
  Serial.println("wakeUp end: "); 
  Serial.print("digitalRead: "); Serial.println(digitalRead(PIN_BUTTON_ENTER));
  
//  if (ui8_EnterButtonStatePressedTime == 3){softReset();} else {goToSleep();}
}
*/

/************************************************************************************************
 *    void INT0_ISR (void)
 *
 *     Function: Interrupt 0 service routine. (Arduino pin D2)
 *               Arousing curiosity about the normal run.
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
/* 
void INT0_ISR (void)
{
  sleep_disable();              // cancel sleep as a precaution
  detachInterrupt(0);           // detach interrupt so it only occurs once precautionary while we do other stuff
  delay(1000);
  Serial.println("Wake up");
  delay(1000);
//  pinMode(HEARDBEAT_LED_OF_ARDUINO, OUTPUT);   // setup the heardbeat LED pin as output
//  softReset();
//  wakeUp();
}  
*/

/************************************************************************************************
 *    void goToSleep (void)
 *
 *     Function: Software moderate switching off of curiosity. (Power down).
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
/*
void goToSleep (void)
{ 
  Serial.println("Go to sleep");
  display.ssd1306_command(SSD1306_DISPLAYOFF);            // switch off OLED display
  switchFlashlight(OFF);                                  // switch off all neopix LED
  digitalWrite(HEARDBEAT_LED_OF_ARDUINO, LOW);            // switch off the heardbeat led
  pinMode(HEARDBEAT_LED_OF_ARDUINO, INPUT);               // setup the pin as input (power save)
  ADCSRA &= ~(1<<ADEN);                                   // switch Analog to Digitalconverter OFF
  ui8_SystemState = WAKEUP_STARTUP;
  delay (1000); 
  
  ADCSRA = 0;                                             // disable ADC
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);  
  sleep_enable();
  
  noInterrupts();                                         // Do not interrupt before we go to sleep, or the ISR will detach interrupts and we won't wake.
  attachInterrupt(0, INT0_ISR, FALLING);                  // will be called when pin D2 goes low
  EIFR = bit (INTF0);                                     // clear flag for interrupt 0
 
  MCUCR = bit (BODS) | bit (BODSE);                       // turn off brown-out enable in software BODS must be set to one and BODSE must be set to zero within four clock cycles
  MCUCR = bit (BODS);                                     // The BODS bit is automatically cleared after three clock cycles
 
  // We are guaranteed that the sleep_cpu call will be done as the processor executes the next instruction after interrupts are turned on.
  interrupts();                                           // one cycle
  sleep_cpu();                                            // one cycle
}
*/

/************************************************************************************************
 *    void initialButtons(void)
 *
 *     Function: Initial Buttons soft and hardware
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void initialButtons(void)
{
  PREG_BUTTON_DDR &= ~((1<<BIT_BUTTON_UP) | (1<<BIT_BUTTON_DOWN) | (1<<BIT_BUTTON_LEFT) | (1<<BIT_BUTTON_RIGHT)); // all navigation button pins as input. No arduino pinMode() used.
  PREG_BUTTON_PORT |= ((1<<BIT_BUTTON_UP) | (1<<BIT_BUTTON_DOWN) | (1<<BIT_BUTTON_LEFT) | (1<<BIT_BUTTON_RIGHT)); // all navigation button inputs with pullup on. No arduino pinMode() used.
  pinMode(PIN_BUTTON_ENTER, INPUT);    // Enter button pin as input (ext. pullup) 

  ui8_ButtonInputRegister     = 0x00;  // current button input result register pullup on input = high no key presswed!
  ui8_OldButtonInputRegister  = 0x00;  // previous button input result register pullup on input = high no key presswed!
  ui8_ButtonPressedRegister   = 0x00;  // button pressed result register
  ui8_ButtonChangeRegister    = 0x00;  // button state change result register
  ui8_ButtonReleaseRegister   = 0x00;  // button release result register
  for(uint8_t ui8_Loop = 0; ui8_Loop < 5; ui8_Loop++)
  {
    ui8ary_ButtonPressedPastTime[ui8_Loop] = 0x00;  // clear how long has the button been pressed?
  }
  ui32_Past = millis();
}


/************************************************************************************************
 *    uint8_t scanButtons(void)
 *
 *     Function: Gets the current status of the buttons and compares it with the previous one.
 *               The edge detection and more are than calculated from this and stored in the 
 *               appropriate register.
 *
 *    Handovers: void
 *       Return: uint8_t = ui8_ButtonChangeRegister => if > 0 then there are any changes
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

uint8_t scanButtons(void)
{
//  uint32_t ui32_Now = millis(); 
//  uint32_t ui32_PastTime = ui32_Now - ui32_Past;                                        // get past time
  
//  if (ui32_PastTime == BUTTON_DEBOUNCE_DELAY)
  {
    ui8_ButtonInputRegister = (~PREG_BUTTON_PIN & ((1<<BIT_BUTTON_UP) | (1<<BIT_BUTTON_DOWN) | (1<<BIT_BUTTON_LEFT) | (1<<BIT_BUTTON_RIGHT))); // read invertet navigation button pins (active low) and mask
    if (!digitalRead(PIN_BUTTON_ENTER))                                                 // read the inverted enter button pin (active low)
      ui8_ButtonInputRegister |= (1<<BIT_BUTTON_ENTER);                                 // if active, then set the corresponding bit in the input register 
    else
      ui8_ButtonInputRegister &= ~(1<<BIT_BUTTON_ENTER);                                // else clear the corresponding bit in the input register 
    
    // edge handling
    ui8_ButtonChangeRegister = ui8_OldButtonInputRegister ^ ui8_ButtonInputRegister;    // any button input has changed
    ui8_ButtonPressedRegister = ui8_ButtonChangeRegister & ui8_ButtonInputRegister;     // any button input has pressed
    ui8_ButtonReleaseRegister = ui8_ButtonChangeRegister & ui8_OldButtonInputRegister;  // any button input has released
    ui8_OldButtonInputRegister = ui8_ButtonInputRegister;                               // store current status in to the ui8_OldKeyInputRegister

    // past time handling
    if (ui8_ButtonInputRegister & (1<<BIT_BUTTON_UP))    ui8ary_ButtonPressedPastTime[BIT_BUTTON_UP]++;    else ui8ary_ButtonPressedPastTime[BIT_BUTTON_UP] = 0x00;
    if (ui8_ButtonInputRegister & (1<<BIT_BUTTON_DOWN))  ui8ary_ButtonPressedPastTime[BIT_BUTTON_DOWN]++;  else ui8ary_ButtonPressedPastTime[BIT_BUTTON_DOWN] = 0x00;
    if (ui8_ButtonInputRegister & (1<<BIT_BUTTON_LEFT))  ui8ary_ButtonPressedPastTime[BIT_BUTTON_LEFT]++;  else ui8ary_ButtonPressedPastTime[BIT_BUTTON_LEFT] = 0x00;
    if (ui8_ButtonInputRegister & (1<<BIT_BUTTON_RIGHT)) ui8ary_ButtonPressedPastTime[BIT_BUTTON_RIGHT]++; else ui8ary_ButtonPressedPastTime[BIT_BUTTON_RIGHT] = 0x00;
    if (ui8_ButtonInputRegister & (1<<BIT_BUTTON_ENTER)) ui8ary_ButtonPressedPastTime[BIT_BUTTON_ENTER]++; else ui8ary_ButtonPressedPastTime[BIT_BUTTON_ENTER] = 0x00;
    
//    ui32_PastTime = ui32_Now;                                                           // update past = now is the new past
  }

  return ui8_ButtonChangeRegister;
}


/************************************************************************************************
 *    uint8_t getButtonState(uint8_t ui8_Button, uint8_t ui8_State)
 *
 *     Function: Check the status of the selected button. 
 *
 *    Handovers: uint8_t ui8_Button = 0 - 5 => (x<<1) => UP, Down, Left, Right and Enter 
 *               uint8_t ui8_State = 0 = buttons status | 1 = changed | 2 = pressed | 3 = released
 *       Return: uint8_t = masked and selected register value
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
uint8_t getButtonState(uint8_t ui8_Button, uint8_t ui8_State)
{
  uint8_t ui8_Result = 0x00;
  
  switch(ui8_State)
  {
    case 0: if (ui8_ButtonInputRegister & ui8_Button) ui8_Result   = (ui8_ButtonInputRegister & ui8_Button);   break;  // return the masket input register   (static state)
    case 1: if (ui8_ButtonChangeRegister & ui8_Button) ui8_Result  = (ui8_ButtonChangeRegister & ui8_Button);  break;  // return the masket change register  (edge detected)
    case 2: if (ui8_ButtonPressedRegister & ui8_Button) ui8_Result = (ui8_ButtonPressedRegister & ui8_Button); break;  // return the masket pressed register (negative edge)
    case 3: if (ui8_ButtonReleaseRegister & ui8_Button) ui8_Result = (ui8_ButtonReleaseRegister & ui8_Button); break;  // return the masket release register (positive edge)
   default: if (ui8_ButtonInputRegister & ui8_Button) ui8_Result   = (ui8_ButtonInputRegister & ui8_Button);   break;  // return the masket input register   (static state)
  }
  return ui8_Result;
}


/************************************************************************************************
 *    uint16_t getButtonPastTime(uint8_t ui8_Button)
 *
 *     Function: Get the past time of the selected button. 
 *
 *    Handovers: uint8_t ui8_Button = 0 - 5 => UP, Down, Left, Right and Enter 
 *       Return: uint16_t = past time => debounce time * past time counter value => ms
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
uint16_t getButtonPastTime(uint8_t ui8_Button)
{
  uint16_t ui8_Result = ui8ary_ButtonPressedPastTime[ui8_Button] * BUTTON_DEBOUNCE_DELAY; // debounce time * past time counter value typ max. 20ms * 256 = 5,12s
  return ui8_Result;
}


/************************************************************************************************
 *    void initialNeopixel(void)
 *
 *     Function: INITIALIZE NeoPixel strip object (REQUIRED),
 *               Turn OFF all pixels ASAP
 *               and set the brightness of the strip NeoPixel
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void initialNeopixel(void)
{
  NeoPixelStrip.begin();                      // INITIALIZE NeoPixel strip object (REQUIRED)
  NeoPixelStrip.show();                       // Turn OFF all pixels ASAP
  NeoPixelStrip.setBrightness(BRIGHTNESS);    // set the brightness of the strip NeoPixel
}


/************************************************************************************************
 *    void initialDisplay(void)
 *
 *     Function: INITIALIZE OLED display object (REQUIRED)
 *               and clear screen
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void initialDisplay(void)
{
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS))  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);                                                // Don't proceed, loop forever
  }

  // Show initial display buffer contents on the screen --
  // the library initializes this with an Adafruit splash screen.
  display.display();
  delay(1000);                // Pause for 1 seconds
  display.clearDisplay();     // Clear the buffer
  drawSplashScreen();         // Splash screen of curiosity
  delay(3000);                // Pause for 3 seconds
}


/************************************************************************************************
 *    void initialUserText(void)
 *
 *     Function: INITIALIZE User Text functions
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
/* 
void initialUserText(void)
{
  display.setFont(&FreeSans9pt7b);
  display.setTextColor(SSD1306_WHITE);
  display.setTextSize(2);
  display.setTextWrap(false);
  int16_XPos    = display.width();
  int16_XPosMin = -12 * strlen(chrary_Message); // 12 = 6 pixels/character * text size 2
  int16_YPos    = display.height() + 10;        // so it starts out at the point on the screen, not below
  int16_YPosMin = -12 * strlen(chrary_Message); // 12 = 6 pixels/character * text size 2  
}
*/

/************************************************************************************************
 *    void drawPixelFromEEPROM(uint8_t ui8_PosX, uint8_t ui8_PosY, uint8_t ui8_ImgW, uint8_t ui8_ImgH, uint8_t ui8_Type, uint16_t ui16_FileSize)
 *
 *     Function: Draw Images from external EEPROM to the OLED display
 *
 *    Handovers: uint8_t ui8_PosX => 
 *               uint8_t ui8_PosY => 
 *               uint8_t ui8_ImgH => 
 *               uint8_t ui8_ImgW => 
 *               uint8_t ui8_Type => 
 *               uint16_t ui16_FileSize =>  
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void drawPixelFromEEPROM(uint8_t _ui8_PosX, uint8_t _ui8_PosY, uint8_t _ui8_ImgW, uint8_t _ui8_ImgH, uint8_t _ui8_Type, uint16_t _ui16_FileSize) // erweitern mit übergabe parametern
{
  uint8_t _ui8_ColLoop = 0, _ui8_RowLoop = 0;
  uint16_t _ui16_EEPROMAddress;

           _ui8_ColLoop = _ui8_ImgH;  // just for fun no only temporary for compiler
           _ui8_ColLoop = 0;

  switch(_ui8_Type)
  {
     case 0: _ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_LOGO; break; 
     case 1: _ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_GAME_IMAGES; break;
     case 2: _ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_IMAGE; break;
    default: _ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_LOGO; break;
  }

  uint16_t _ui16_EEPROMFileSize = _ui16_FileSize; // (_ui8_ImgW * _ui8_ImgH)/8; _ui16_FileSize; //688; // Byte
  uint16_t _ui16_EEPROMLastByteOfFile = _ui16_EEPROMAddress + ((_ui16_EEPROMFileSize));
   uint8_t _ui8_ScreenBufferSize = _ui8_ImgW / 8;
   uint8_t _ui8ary_ScreenBuffer[_ui8_ScreenBufferSize + 1]; // 16 for Logo

  // write line by line
//  cli(); // disable all interrupts
  for (uint16_t _ui16_EEPROMAddressLoop = _ui16_EEPROMAddress; _ui16_EEPROMAddressLoop <= _ui16_EEPROMLastByteOfFile; _ui16_EEPROMAddressLoop++)
  {
    if (_ui8_ColLoop < (_ui8_ScreenBufferSize-1)) 
    {
      _ui8ary_ScreenBuffer[_ui8_ColLoop] = readEEPROM(_ui16_EEPROMAddressLoop);
      _ui8_ColLoop++;
    } 
    else 
    {
      _ui8ary_ScreenBuffer[_ui8_ColLoop] = readEEPROM(_ui16_EEPROMAddressLoop);
      display.drawBitmap(_ui8_PosX, _ui8_PosY + _ui8_RowLoop, _ui8ary_ScreenBuffer, _ui8_ImgW, 1, SSD1306_WHITE);
      _ui8_RowLoop++;
      _ui8_ColLoop = 0;
    }
  }
//  sei(); // enable all interrupts
}


/************************************************************************************************
 *    void drawImageFromEEPROM(uint8_t _ui8_ImageNumber) 
 *
 *     Function: Draw user images from external EEPROM to the OLED display
 *     
 *         Note: Image size if always 128 x 64 pixel use the "Store avatar in external EEPROM" tool.
 *
 *    Handovers: uint8_t _ui8_ImageNumber =>  by EEPROM Type = 25LC128 every set to 0 | by EEPROM Type 25LC512 = 0 => first image | 1 => second image
 *
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void drawImageFromEEPROM(uint8_t _ui8_ImageNumber) 
{
  uint8_t __ui8_ColLoop = 0, __ui8_RowLoop = 0;
  uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_IMAGE + (_ui8_ImageNumber * USER_IMAGE_FILESIZE);
  
  uint16_t __ui16_EEPROMLastByteOfFile = __ui16_EEPROMAddress + ((USER_IMAGE_FILESIZE)-1);
  uint8_t __ui8ary_ScreenBuffer[16];

  for (__ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_IMAGE + (_ui8_ImageNumber * USER_IMAGE_FILESIZE); __ui16_EEPROMAddress <= __ui16_EEPROMLastByteOfFile; __ui16_EEPROMAddress++)  // write line by line
  {
    if (__ui8_ColLoop < 15)
    {
      __ui8ary_ScreenBuffer[__ui8_ColLoop] = readEEPROM(__ui16_EEPROMAddress);
      __ui8_ColLoop++;
    }
    else
    {
      __ui8ary_ScreenBuffer[__ui8_ColLoop] = readEEPROM(__ui16_EEPROMAddress);
      display.drawBitmap(0, __ui8_RowLoop, __ui8ary_ScreenBuffer, 128, 1, WHITE);
      __ui8_RowLoop++;
      __ui8_ColLoop = 0;
    }
  }
}


/************************************************************************************************
 *    void drawSplashScreen(void)
 *
 *     Function: Display the splash screen on the OLED display
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void drawSplashScreen(void)
{
  display.clearDisplay();                                               // Make sure the display is cleared
  drawPixelFromEEPROM(0, 0, 128, 43, SPLASH_SCREEN_IMAGE, 688);
  display.setFont();
  display.setTextSize(1);                                               // Draw 1X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(30, 35);  display.println(F("is the genesis"));
  display.setCursor(30, 45);  display.println(F("of every science"));
  display.display();                                                    // Update the display
}


/************************************************************************************************
 *    EOF System core functions
 ************************************************************************************************/



/************************************************************************************************
 *    BOF User Text functions
 *
 *     Function: Functions for user text handling
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
/************************************************************************************************
 *    void scrollLongText(uint8_t &_ui8_Animation, uint8_t &_ui8_FontSize, uint8_t _ui8_ScrollSpeed, String &_str_Message)
 *
 *     Function: Scroll user text on display.
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void scrollLongText(void)
{
  uint8_t __ui8_ScrollSpeed = 2;
  uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_TEXT + (ui8_ShowTextNumber * (4 + MAX_LENGTH_PER_TEXT));               // address of first byte of the text header 1 Byte Animation + 1 Byte Font Size + 1 Byte Length + max 29 Byte char

  uint8_t __ui8_Animation = readEEPROM(__ui16_EEPROMAddress);                           // get animations type from eeprom
  __ui16_EEPROMAddress++;                                                               // increment directory address to the next 

  uint8_t __ui8_FontSize = readEEPROM(__ui16_EEPROMAddress);                            // get selected font size from eeprom
  __ui16_EEPROMAddress++;                                                               // increment directory address to the next 

  uint8_t __ui8_TextLength = readEEPROM(__ui16_EEPROMAddress);                          // get the real length of user text from eeprom
  __ui16_EEPROMAddress++;                                                               // increment directory address to the next 
  
  __ui16_EEPROMAddress++;                                                               // uint8_t ui8_dummy = updateEEPROM(__ui16_EEPROMAddress); // reserve for future use
  __ui16_EEPROMAddress++;                                                               // increment directory address to the next 

  char chrary_UserTextData[__ui8_TextLength + 1];                                       // temporary char array for receiving the text data from eeprom
  
  for (uint8_t __ui8_CharPos = 0; __ui8_CharPos < __ui8_TextLength; __ui8_CharPos++)
  {
    chrary_UserTextData[__ui8_CharPos] = readEEPROM(__ui16_EEPROMAddress);
    __ui16_EEPROMAddress++;                                                             // increment directory address to the next 
  }
  chrary_UserTextData[__ui8_TextLength] = '\0';
  
  static int16_t __i16_XPos;                                                            // Variables declared as static will only be created and initialized the first time a function is called.
  int16_t __i16_TargetXPos =  -6 * __ui8_FontSize * sizeof(chrary_UserTextData);        // __i16_TargetXPos = pixel/character * font size * length of message
  
  display.clearDisplay();

  // Scroll part of the screen
  if (__ui8_FontSize < 1) __ui8_FontSize = 1;
  display.setTextSize(__ui8_FontSize);
  switch(__ui8_FontSize)
  {
    case 1: display.setCursor(__i16_XPos,29); break;
    case 2: display.setCursor(__i16_XPos,25); break;
    case 3: display.setCursor(__i16_XPos,22); break;
    case 4: display.setCursor(__i16_XPos,18); break;
  }
  display.setTextColor(WHITE);
  display.setTextWrap(false);
  display.print(chrary_UserTextData);
  display.display();
  switch(__ui8_Animation)
  {
    case 0: { __i16_XPos = 0; } break;                                                         // no scroll
    case 1: { if (__i16_XPos < __i16_TargetXPos) { __i16_XPos = display.width(); } else { __i16_XPos -= __ui8_ScrollSpeed; } } break; // scroll left
    case 2: { if (__i16_XPos < display.width()) { __i16_XPos += __ui8_ScrollSpeed; } else { __i16_XPos = __i16_TargetXPos; } } break; // scroll right
  }
}


/************************************************************************************************
 *    EOF User Text functions
 ************************************************************************************************/




/************************************************************************************************
 *    BOF System Power functions
 *
 *     Function: Functions for system power handling
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
/************************************************************************************************
 *    void initialVBATMeasurement(void)
 *
 *     Function: INITIALIZE VBAT measurement
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void initialVBATMeasurement(void)
{
  pinMode(VBAT_MEASUREMENT_ON, OUTPUT);     // set the voltage divider source as output
  analogReference(INTERNAL);                // set the ADC reference to internal 1.1V reference
  digitalWrite(VBAT_MEASUREMENT_ON, HIGH);  // power on on the voltage divider
  dummyReadingsForStabilizeOfADC();         // make readings but don't use them to ensure good reading after reference change
}


/************************************************************************************************
 *    void dummyReadingsForStabilizeOfADC(void)
 *
 *     Function: INITIALIZE ADC after reference change
 *
 *    Handovers: void
 *       Return: void
 *
 *  Description: This function makes 8 ADC measurements but does nothing with them
 *               Since after a reference change the ADC can return bad readings at first.
 *               This function is used to get rid of the first 8 readings to ensure an 
 *               accurate one is displayed.
 *               
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void dummyReadingsForStabilizeOfADC(void)
{
  for(uint8_t ui8_Loop = 0; ui8_Loop < 8; ui8_Loop++)
  {
    analogRead(VBAT_MEASUREMENT_INPUT);
    delay(1);
  }
}


/************************************************************************************************
 *    void vbatMeasurement(void)
 *
 *     Function: VBAT Measurement
 *
 *    Handovers: void
 *       Return: void
 *
 *  Description: This function takes 16 ADC measurements with 10-bit resolutions and adds them together. 
 *               Then the average is calculated and divided by four. This deletes the two least significant bits. 
 *               As a result, 2-bit LSB noise is eliminated. 
 *               
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
uint16_t vbatMeasurement(void)    
{
  uint16_t ui16_Result = 0;
  for(uint8_t ui8_Loop = 0; ui8_Loop < 16; ui8_Loop++)  // 16 measurements with 10 bits
  {
    ui16_Result += analogRead(VBAT_MEASUREMENT_INPUT);
    delay(1);
  }
  return (ui16_Result >> 6);      // div 16 (averaging in 10 bit) and delete 2 bit for the eliminate the LSB noise
}


/************************************************************************************************
 *    float convertToVolt(float _flo_VREV, uint16_t _ui16_ADCInput, uint16_t _ui16_ADCFinalyResolution, float _flo_VoltageDividerRatio) 
 *
 *     Function: VBAT Measurement
 *
 *    Handovers: float _flo_VREV = true reference voltage => 1.075
 *               uint16_t _ui16_ADCInput = ADC value 10 or 8 bit
 *               uint16_t _ui16_ADCFinalyResolution = 1024 for 10 bit or 256 for 8 bit
 *               float _flo_VoltageDividerRatio = (R9 + R10)/R10 => (29k + 10k)/10k ~> 4.9 or 1 = no ratio
 *               
 *       Return: float
 *
 *  Description: This function convers the ADC level integer value into a useful voltage value.
 *               The inputs are the measured ADC value and the ADC reference voltage level
 *               The formula used was obtained from the data sheet:
 *               ((ADC value / 1024) * ref voltage) * ration) or ((ADC value / 256) x ref voltage) * ration)
 *               
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 /*
float convertToVolt(float _flo_VREV, uint16_t _ui16_ADCInput, uint16_t _ui16_ADCFinalyResolution, float _flo_VoltageDividerRatio) 
{
  return ((((float)_ui16_ADCInput / _ui16_ADCFinalyResolution) * _flo_VREV) * _flo_VoltageDividerRatio);
}
*/

/************************************************************************************************
 *    void vbatDisplay (void)
 *
 *     Function: VBAT Measurement display
 *
 *    Handovers: void
 *       Return: void
 *
 *  Description: The measured input VBAT and VCC are displayed as voltage levels.
 *               
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 /*
void vbatDisplay (void)
{
  uint16_t ui16_VBATADC = vbatMeasurement();
  float flo_VBATVoltage = convertToVolt(VBAT_REF,ui16_VBATADC, ADC_FINALY_RESOLUTION, VOLTAGE_DIVIDER_RATIO);
  display.clearDisplay();
  delay(200);
  display.setFont();
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(10, 0);
  display.println("CURIOSITY VBAT");
  display.drawFastHLine(0, 12, 128, SSD1306_WHITE);
  display.setCursor(10, 20);
  display.print("VBATIN: "); display.print(convertToVolt(VBAT_REF,ui16_VBATADC, ADC_FINALY_RESOLUTION, 1),3); display.println(" V");
  display.setCursor(10, 30);
  display.print("  VBAT: "); display.print(convertToVolt(VBAT_REF,ui16_VBATADC, ADC_FINALY_RESOLUTION, VOLTAGE_DIVIDER_RATIO),3); display.println(" V");
  vbatGauge (10, 40, flo_VBATVoltage, 3.000, 3.600, 5.000); 
  delay(500);
  display.display(); 
}
*/

/************************************************************************************************
 *    void vbatGauge (uint8_t ui8_PosX, uint8_t ui8_PosY, 
 *                    uint16_t ui16_Value, 
 *                    uint16_t ui16_EmptyBat, uint16_t ui16_LowBat, uint16_t ui16_FullBat )
 *
 *     Function: VBAT Measurement display
 *
 *    Handovers: uint16_t ui16_Value = ADC Value
 *       Return: void
 *
 *  Description: The measured input VBAT and VCC are displayed as gauge levels.
 *               
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 /*
void vbatGauge (uint8_t _ui8_PosX, uint8_t _ui8_PosY, 
                float _flo_VBATVoltage, 
                float _flo_EmptyBat, float _flo_LowBat, float _flo_FullBat )
{
  display.setFont();
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  if (_flo_VBATVoltage < _flo_EmptyBat)
  {
    display.setCursor(_ui8_PosX+6, _ui8_PosY+15);
    display.print("BATTERY IS EMPTY");      
  }               
  else if (_flo_VBATVoltage < _flo_LowBat)
       {
         display.setCursor(_ui8_PosX+6, _ui8_PosY+15);
         display.print("  BATTERY LOW   ");      
       }

  
  float flo_Percent = _flo_VBATVoltage / (_flo_FullBat / 100);
  uint8_t ui8_GaugeBarValue = (uint8_t)(flo_Percent * 0.3); // % * dots/%
  
  // drawgauge
  display.drawRect(_ui8_PosX,     _ui8_PosY,   34, 7, SSD1306_WHITE);   // draw frame
  display.fillRect(_ui8_PosX+34,  _ui8_PosY+2,  2, 3, SSD1306_WHITE);   // draw pole
  display.fillRect(_ui8_PosX+2,   _ui8_PosY+2, ui8_GaugeBarValue, 3, SSD1306_WHITE);   // draw value
  display.setCursor(_ui8_PosX+48, _ui8_PosY);
  display.print(flo_Percent,1); display.print("  %");
}
*/

/************************************************************************************************
 *    uint8_t getSystemPowerLimit(void)
 *
 *     Function: Get the system power limit from the power supply voltage.
 *
 *    Handovers: void
 *       Return: uint8_t ui8_SystemPowerLimit = 0 = no limit, 1 = 25% limit, 2 = 50% limit, 3 = 75% limit, 4 = 100% limit power off
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 /*
uint8_t getSystemPowerLimit(void)
{
  uint8_t ui8_SystemPowerLimit = 0;                                     // 0 = no limit, 1 = 25% limit, 2 = 50% limit, 3 = 75% limit, 4 = 100% limit power off
  uint16_t ui16_VBATADC = vbatMeasurement();                            // calc voltage ((ADC value / 256) * ref voltage) * ration => ((X / 256) * 1.073) * 4.9

  // calc HEX value ((Volt / ration) / ref voltage) * 256 => ((V / 4.9) / 1.073) * 256
  
  if (ui16_VBATADC < 194) ui8_SystemPowerLimit = 1;                     // ((3.984 / 4.9) / 1.073) * 256
  else if (ui16_VBATADC < 180) ui8_SystemPowerLimit = 2;                // ((3.697 / 4.9) / 1.073) * 256
       else if (ui16_VBATADC < 165) ui8_SystemPowerLimit = 3;           // ((3.389 / 4.9) / 1.073) * 256
            else if (ui16_VBATADC < 146) ui8_SystemPowerLimit = 4;      // ((2.998 / 4.9) / 1.073) * 256

  return ui8_SystemPowerLimit;
}
*/

/************************************************************************************************
 *    EOF System Power functions
 ************************************************************************************************/




/************************************************************************************************
 *    BOF EEPROM functions
 *
 *     Function: Functions for EEPROM handling
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
/************************************************************************************************
 *    void resetExternalEEPROMDirectoryAddress(void)
 *
 *     Function: INITIALIZE extEEPROM file system.
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
/*
String printEEPROMAddressHEX(uint16_t &_ui16_EEPROMAddress)
{
  Serial.print(_ui16_EEPROMAddress < 4096 ? "0" : ""); // print or not a leading zero
  Serial.print(_ui16_EEPROMAddress <  256 ? "0" : ""); // print or not a leading zero
  Serial.print(_ui16_EEPROMAddress <   16 ? "0" : ""); // print or not a leading zero
  Serial.print(_ui16_EEPROMAddress, HEX); Serial.print(" : ");
  return String(chrary_TextData);
}
*/

/************************************************************************************************
 *    uint8_t writeEEPROM(uint16_t ui16_Address, uint8_t ui8_Data)
 *
 *     Function: Write the Data to the eeprom address.
 *
 *    Handovers: uint16_t ui16_Address = The eeprom address to write to.
 *                    uint8_t ui8_Data = The data to be written to.
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void writeEEPROM(uint16_t ui16_Address, uint8_t ui8_Data)
{
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);  // Begin transmission to I2C EEPROM
  Wire.write((int)(ui16_Address >> 8));           // MSB Send memory address as two 8-bit bytes
  Wire.write((int)(ui16_Address & 0xFF));         // LSB Send memory address as two 8-bit bytes
  Wire.write(ui8_Data);                           // Send data to be stored
  Wire.endTransmission();                         // End the transmission
  delay(5);                                       // Add 5ms delay for EEPROM
}


/************************************************************************************************
 *    uint8_t readEEPROM(uint16_t ui16_Address)
 *
 *     Function: Read the Data from the eeprom address.
 *
 *    Handovers: uint16_t ui16_Address = The eeprom address to read to.
 *       Return: uint8_t = Data from the addressed EEPROM memory location.
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

uint8_t readEEPROM(uint16_t ui16_Address)
{
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);        // Begin transmission to I2C EEPROM
  Wire.write((int)(ui16_Address >> 8));                 // MSB Send memory address as two 8-bit bytes
  Wire.write((int)(ui16_Address & 0xFF));               // LSB Send memory address as two 8-bit bytes
  Wire.endTransmission();                               // End the transmission
  Wire.requestFrom((int)EEPROM_I2C_ADDRESS, 1);      // Request one byte of data at current memory address
  uint8_t ui8_Data =  Wire.read();                      // Read the data and assign to variable
  return ui8_Data;                                      // Return the data as function output
}


/************************************************************************************************
 *    uint8_t updateEEPROM(uint16_t ui16_Address, uint8_t ui8_Data)
 *
 *     Function: Update the Data to the eeprom address.
 *
 *    Handovers: uint16_t ui16_Address = The eeprom address to write to.
 *                    uint8_t ui8_Data = The data to be written to.
 *       Return: uint8_t = execution result => 0 = does not need an update | 1 = updated
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
uint8_t updateEEPROM(uint16_t ui16_Address, uint8_t ui8_Data)
{
  uint8_t ui8_Result = 0x00;
  uint8_t ui8_TempData = readEEPROM(ui16_Address);
  if (ui8_TempData != ui8_Data) { writeEEPROM(ui16_Address, ui8_Data); ui8_Result = 0x01;}
  return ui8_Result;
}


/************************************************************************************************
 *    void updateUserStringToEEPROM(uint8_t &_ui8_TextNumber, uint8_t &_ui8_Animation, uint8_t &_ui8_FontSize, String &_str_StrToWrite)
 *
 *     Function: store user string to EEPROM
 *
 *    Handovers: uint8_t &_ui8_TextNumber =
 *               uint8_t &_ui8_Animation =
 *               uint8_t &_ui8_FontSize = 
 *               String &_str_StrToWrite =
 *               
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void updateUserStringToEEPROM(uint8_t &_ui8_TextNumber, uint8_t &_ui8_Animation, uint8_t &_ui8_FontSize, String &_str_StrToWrite)
{
    uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_TEXT + (_ui8_TextNumber * (4 + MAX_LENGTH_PER_TEXT));               // address of first byte of the text header 1 Byte Animation + 1 Byte Font Size + 1 Byte Length + max 29 Byte char
    uint8_t __ui8_TextLength = _str_StrToWrite.length();
  
    updateEEPROM(__ui16_EEPROMAddress, _ui8_Animation);                                 // animations type
    __ui16_EEPROMAddress++;                                                             // increment directory address to the next 
  
    updateEEPROM(__ui16_EEPROMAddress, _ui8_FontSize);                                  // selected font size
    __ui16_EEPROMAddress++;                                                             // increment directory address to the next 
  
    updateEEPROM(__ui16_EEPROMAddress, __ui8_TextLength);                               // the real length of user text
    __ui16_EEPROMAddress++;                                                             // increment directory address to the next 
  
    updateEEPROM(__ui16_EEPROMAddress, 0);                                              // reserve for future use
    __ui16_EEPROMAddress++;                                                             // increment directory address to the next 
    
    for (uint8_t __ui8_CharPos = 0; __ui8_CharPos < __ui8_TextLength; __ui8_CharPos++)
    {
      updateEEPROM(__ui16_EEPROMAddress, _str_StrToWrite[__ui8_CharPos]);
      __ui16_EEPROMAddress++;                                                           // increment directory address to the next 
    }

    #ifdef DEBUG_SERIAL_OUTPUT
      Serial.print(F("_ui8_TextNumber : ")); Serial.println(_ui8_TextNumber);
      Serial.print(F("__ui16_EEPROMAddress : ")); Serial.println(__ui16_EEPROMAddress);
      Serial.print(F("_ui8_Animation : ")); Serial.println(_ui8_Animation);
      Serial.print(F("_ui8_FontSize : ")); Serial.println(_ui8_FontSize);
      Serial.print(F("__ui8_TextLength : ")); Serial.println(__ui8_TextLength);
      Serial.print(F("_str_StrToWrite : ")); Serial.println(_str_StrToWrite);
    #endif

}


/************************************************************************************************
 *    String readUserStringFromEEPROM(uint8_t &_ui8_TextNumber, uint8_t &_ui8_Animation, uint8_t &_ui8_FontSize)
 *
 *     Function: Read the string from the file stored in the EEPROM.
 *
 *    Handovers: uint8_t ui8_FileNumber = The file number corresponds to a simple form of file name.
 *       Return: String = text from file
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

//String readUserStringFromEEPROM(uint8_t &_ui8_TextNumber, uint8_t &_ui8_Animation, uint8_t &_ui8_FontSize)
void readUserStringFromEEPROM(uint8_t &_ui8_TextNumber, uint8_t &_ui8_Animation, uint8_t &_ui8_FontSize, String &_str_ToRead)
{
  uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_TEXT + (_ui8_TextNumber * (4 + MAX_LENGTH_PER_TEXT));               // address of first byte of the text header 1 Byte Animation + 1 Byte Font Size + 1 Byte Length + max 29 Byte char
  uint8_t __ui8_TextLength;

  _ui8_Animation = readEEPROM(__ui16_EEPROMAddress);                                  // get animations type from eeprom
  __ui16_EEPROMAddress++;                                                             // increment directory address to the next 

  _ui8_FontSize = readEEPROM(__ui16_EEPROMAddress);                                   // get selected font size from eeprom
  __ui16_EEPROMAddress++;                                                             // increment directory address to the next 

  __ui8_TextLength = readEEPROM(__ui16_EEPROMAddress);                                // get the real length of user text from eeprom
  __ui16_EEPROMAddress++;                                                             // increment directory address to the next 
  
  __ui16_EEPROMAddress++;                                                             // uint8_t ui8_dummy = updateEEPROM(__ui16_EEPROMAddress); // reserve for future use
  __ui16_EEPROMAddress++;                                                             // increment directory address to the next 

  char chrary_UserTextData[__ui8_TextLength + 1];                                     // temporary char array for receiving the text data from eeprom
  
  for (uint8_t __ui8_CharPos = 0; __ui8_CharPos < __ui8_TextLength; __ui8_CharPos++)
  {
    chrary_UserTextData[__ui8_CharPos] = readEEPROM(__ui16_EEPROMAddress);
    __ui16_EEPROMAddress++;                                                           // increment directory address to the next 
  }
  chrary_UserTextData[__ui8_TextLength] = '\0';

  #ifdef DEBUG_SERIAL_OUTPUT
    Serial.print(F("_ui8_Animation: ")); Serial.println(_ui8_Animation);
    Serial.print(F("_ui8_FontSize: ")); Serial.println(_ui8_FontSize);
    Serial.print(F("__ui8_TextLength: ")); Serial.println(__ui8_TextLength);
    Serial.print(F("String: ")); Serial.println(chrary_UserTextData); Serial.println();
  #endif

 // return String(chrary_UserTextData);
    _str_ToRead = String(chrary_UserTextData);
}


/************************************************************************************************
 *    void clearUserStringToEEPROM(uint8_t _ui8_TextNumber)
 *
 *     Function: clear a user text
 *
 *    Handovers: uint8_t _ui8_TextNumber = 64kbit EEPROM => 0  - 7 | 16kbit EEPROM => 0 - 3 
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void clearUserStringToEEPROM(uint8_t _ui8_TextNumber)
{
  uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_TEXT + (_ui8_TextNumber * (4 + MAX_LENGTH_PER_TEXT));               // address of first byte of the text header 1 Byte Animation + 1 Byte Font Size + 1 Byte Length + max 29 Byte char

  for (uint8_t __ui8_CharPos = 0; __ui8_CharPos < MAX_LENGTH_PER_TEXT + 4; __ui8_CharPos++)
  {
    updateEEPROM(__ui16_EEPROMAddress, 0xFF);                                         // the equal value of a new empty eeprom
    __ui16_EEPROMAddress++;                                                           // increment directory address to the next 
  }
}


/************************************************************************************************
 *    void updateLedShow (uint8_t _ui8_ShowNumber, uint8_t _ui8_Frames, uint16_t _ui16_ShowPause, uint8_t _ui8_Leds)
 *
 *     Function: store a user led show frame to the EEPROM
 *
 *    Handovers: uint8_t _ui8_ShowNumber = 64kbit EEPROM => 0  - 3 | 16kbit EEPROM => 0 - 1
 *               uint8_t _ui8_Frames = Count of used frames for this show max. is 32
 *               uint16_t _ui16_ShowPause = 0 - 65535 ms
 *               uint8_t _ui8_Leds = 0 - 3 max. 4 (default)
 *               
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void updateLedShow (uint8_t _ui8_ShowNumber, uint8_t _ui8_Frames, uint16_t _ui16_ShowPause, uint8_t _ui8_Leds)
{ 
  uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_SHOW + (_ui8_ShowNumber * LED_SHOW_BLOCK_SIZE);               // address of first byte of the show header
  #ifdef DEBUG_SERIAL_OUTPUT
    Serial.print(F(" : _ui8_Frames : ")); Serial.println(_ui8_Frames);
  #endif
  updateEEPROM(__ui16_EEPROMAddress, _ui8_Frames);                                                              // Frames in the annimation
  __ui16_EEPROMAddress++;
  #ifdef DEBUG_SERIAL_OUTPUT
    Serial.print(F(" : _ui16_ShowPause : ")); Serial.println(_ui16_ShowPause >> 8);
  #endif
  updateEEPROM(__ui16_EEPROMAddress, _ui16_ShowPause >> 8);                                                     // Delay after show high byte
  __ui16_EEPROMAddress++;
  #ifdef DEBUG_SERIAL_OUTPUT
    Serial.print(F(" : _ui16_ShowPause : ")); Serial.println(_ui16_ShowPause & 0x00FF);
  #endif
  updateEEPROM(__ui16_EEPROMAddress, _ui16_ShowPause);                                                          // Delay after show low byte
  __ui16_EEPROMAddress++;
  #ifdef DEBUG_SERIAL_OUTPUT
    Serial.print(F(" : _ui8_Leds : ")); Serial.println(_ui8_Leds);
  #endif
  updateEEPROM(__ui16_EEPROMAddress, _ui8_Leds);                                                                // Leds per strip
}


/************************************************************************************************
 *    void clearShow (uint8_t _ui8_ShowNumber)
 *
 *     Function: clear a user led show from the EEPROM
 *
 *    Handovers: uint8_t _ui8_ShowNumber = 64kbit EEPROM => 0  - 3 | 16kbit EEPROM => 0 - 1
 *               
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void clearShow (uint8_t _ui8_ShowNumber)
{
  updateLedShow(_ui8_ShowNumber, MAX_FRAMES_PER_SHOW, 0, COUNT_OF_LED);
  
  uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_SHOW + FRAME_DATA_OFFSET + (_ui8_ShowNumber * LED_SHOW_BLOCK_SIZE);  // address of first byte of the show

  for(uint8_t __ui8_FrameLoop = 0; __ui8_FrameLoop < MAX_FRAMES_PER_SHOW; __ui8_FrameLoop++)
  {
    for(uint8_t __ui8_LedLoop = 0; __ui8_LedLoop < COUNT_OF_LED; __ui8_LedLoop++) 
    {
      for(uint8_t __ui8_LEDColorLoop = 0; __ui8_LEDColorLoop < COUNT_OF_LED_COLORS; __ui8_LEDColorLoop++)
      {
        updateEEPROM(__ui16_EEPROMAddress, 0);              // Write eeprom with data clear frames all led off
        __ui16_EEPROMAddress++;
      }
    }
    updateEEPROM(__ui16_EEPROMAddress, 0);                 // Frame delay low byte
    __ui16_EEPROMAddress++;
    updateEEPROM(__ui16_EEPROMAddress, 0);                 // Frame delay high byte
    __ui16_EEPROMAddress++;
  }
  Serial.println(); Serial.print(F("Delete Show : ")); Serial.print(_ui8_ShowNumber); Serial.print(F(" cleared."));
}




/************************************************************************************************
 *    void loadLedShow (uint8_t _ui8_ShowNumber)
 *
 *     Function: Load a user led show from the EEPROM
 *
 *    Handovers: uint8_t _ui8_ShowNumber = 64kbit EEPROM => 0  - 3 | 16kbit EEPROM => 0 - 1
 *               
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void loadLedShow (uint8_t _ui8_ShowNumber)
{
  uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_SHOW + (_ui8_ShowNumber * LED_SHOW_BLOCK_SIZE);       // address of first byte of the show header
  
  ui8_EepromProgramData[FRAMES] = readEEPROM(__ui16_EEPROMAddress); __ui16_EEPROMAddress++;                           // Get amount of frames
  ui8_EepromProgramData[SHOW_DELAY] = readEEPROM(__ui16_EEPROMAddress); __ui16_EEPROMAddress++;                       // Get delay high byte per frame
  ui8_EepromProgramData[SHOW_DELAY + 1] = readEEPROM(__ui16_EEPROMAddress); __ui16_EEPROMAddress++;                   // Get delay low byte per frame
  ui8_EepromProgramData[FRAME_LED_COUNT] = readEEPROM(__ui16_EEPROMAddress); __ui16_EEPROMAddress++;                  // Get leds per strip

  #ifdef DEBUG_SERIAL_OUTPUT
    Serial.println(F("loadLedShow")); Serial.println();
    Serial.print(F("__ui16_EEPROMAddress: ")); Serial.println(__ui16_EEPROMAddress);
    Serial.print(F("[FRAMES]: ")); Serial.println(ui8_EepromProgramData[FRAMES]);
    Serial.print(F("[SHOW_DELAY]     : ")); Serial.println(ui8_EepromProgramData[SHOW_DELAY]);
    Serial.print(F("[SHOW_DELAY + 1] : ")); Serial.println(ui8_EepromProgramData[SHOW_DELAY + 1]);
    Serial.print(F("[FRAME_LED_COUNT]: ")); Serial.println(ui8_EepromProgramData[FRAME_LED_COUNT]);
    Serial.println();
  #endif
}

/************************************************************************************************
 *    void getAvailableLedShows(void)
 *
 *     Function: Get the avalaible user led show from the EEPROM
 *
 *    Handovers: void
 *               
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void getAvailableLedShows(void)
{
  uint8_t __ui8_Fault;

  ui8_AvailableLEDShow = 0;

  for (uint8_t __ui8_ShowNumber = 0; __ui8_ShowNumber < MAX_OF_SHOWS; __ui8_ShowNumber++)
  {
    __ui8_Fault = 0;
    loadLedShow (__ui8_ShowNumber);
    if ((ui8_EepromProgramData[FRAMES] < 1) || (ui8_EepromProgramData[FRAMES] > MAX_FRAMES_PER_SHOW)) __ui8_Fault = 1;
    uint16_t __ui16_Delay = (ui8_EepromProgramData[SHOW_DELAY] << 8) || ui8_EepromProgramData[SHOW_DELAY + 1];
    if (__ui16_Delay > 10000)  __ui8_Fault = 1;
    if ((ui8_EepromProgramData[FRAME_LED_COUNT] < 4) || (ui8_EepromProgramData[FRAME_LED_COUNT] > 4)) __ui8_Fault = 1;
    if (__ui8_Fault == 0) ui8_AvailableLEDShow |= (1<<__ui8_ShowNumber);
  }
//  Serial.print(F("ui8_AvailableLEDShow: ")); Serial.println(ui8_AvailableLEDShow);

}


/************************************************************************************************
 *    void getNextLedShowFrame (uint8_t _ui8_ShowNumber)
 *
 *     Function: Load the next frame of a user led show whit _ui8_ShowNumber from the EEPROM
 *
 *    Handovers: uint8_t _ui8_ShowNumber = 64kbit EEPROM => 0  - 3 | 16kbit EEPROM => 0 - 1
 *               
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void getNextLedShowFrame (uint8_t _ui8_ShowNumber)
{
  uint8_t __ui8_EepromLedColor[COUNT_OF_LED_COLORS];                    // Contains R, G, B, W values

  static uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_SHOW + FRAME_DATA_OFFSET + (_ui8_ShowNumber * LED_SHOW_BLOCK_SIZE);  // address of first byte of the show

  #ifdef DEBUG_SERIAL_OUTPUT
    Serial.print(F("EEPROM Address : ")); Serial.print(__ui16_EEPROMAddress); Serial.print(F(" "));
  #endif

  for(uint8_t __ui8_LEDLoop = 0; __ui8_LEDLoop < ui8_EepromProgramData[FRAME_LED_COUNT]; __ui8_LEDLoop++) 
  {
    for(uint8_t __ui8_LEDColorLoop = 0; __ui8_LEDColorLoop < COUNT_OF_LED_COLORS; __ui8_LEDColorLoop++)
    {
       __ui8_EepromLedColor[__ui8_LEDColorLoop] = readEEPROM(__ui16_EEPROMAddress); // Get RGBW data for one led
      #ifdef DEBUG_SERIAL_OUTPUT
         Serial.print(__ui8_EepromLedColor[__ui8_LEDColorLoop]); Serial.print(F(","));
      #endif
       __ui16_EEPROMAddress++;
    }
    NeoPixelStrip.setPixelColor(__ui8_LEDLoop, NeoPixelStrip.Color(__ui8_EepromLedColor[RED_LED], __ui8_EepromLedColor[GREEN_LED], __ui8_EepromLedColor[BLUE_LED], __ui8_EepromLedColor[WHITE_LED]));                                             // Set one led with RGBW value
  }
  NeoPixelStrip.show();  // Display on the strip

  uint16_t __ui16_FrameDelay = readEEPROM(__ui16_EEPROMAddress) << 8;            // Get delay high byte per frame
  __ui16_EEPROMAddress++;
  __ui16_FrameDelay |= readEEPROM(__ui16_EEPROMAddress) & 0xFF;                  // Get delay low byte per frame
  
  #ifdef DEBUG_SERIAL_OUTPUT
    Serial.print(__ui16_FrameDelay);
  #endif
  
  __ui16_EEPROMAddress++;
  delay(__ui16_FrameDelay);                                                       // running frame delay
  
  #ifdef DEBUG_SERIAL_OUTPUT
    Serial.print(F(" EOF FRAME : ")); Serial.println(ui8_FrameLoop);
  #endif
  
  if (ui8_FrameLoop < (ui8_EepromProgramData[FRAMES] - 1)) 
  {
    ui8_FrameLoop++;
  }
  else
  {
    ui8_FrameLoop = 0;
    __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_SHOW + FRAME_DATA_OFFSET + (_ui8_ShowNumber * LED_SHOW_BLOCK_SIZE);      // address of first byte of the show
    delay(uint16_t((ui8_EepromProgramData[SHOW_DELAY]<<8)|(ui8_EepromProgramData[SHOW_DELAY+1] & 0xFF)));           // get two 8 bit values and build 16 bit value set the delay in ms high byte first
    
    #ifdef DEBUG_SERIAL_OUTPUT
      Serial.print(F(" EOF SHOW PAUSE: ")); Serial.println(((ui8_EepromProgramData[SHOW_DELAY]<<8)|(ui8_EepromProgramData[SHOW_DELAY+1] & 0xFF)));
      Serial.print(F(" EOF SHOW : ")); Serial.println((ui8_LEDSystemMode & ACTIVE_LED_SHOW_BIT_MASK) >> 4);
    #endif
  }
}

void clearLedStripe(void)
{
  for(uint8_t __ui8_LEDLoop = 0; __ui8_LEDLoop < 4; __ui8_LEDLoop++) 
  {
    NeoPixelStrip.setPixelColor(__ui8_LEDLoop, NeoPixelStrip.Color(0, 0, 0, 0));                                             // Set one led with 0 for RGBW value
  }
  NeoPixelStrip.show();  // Display on the strip
}

/************************************************************************************************
 *    void copyDataFromFlashToEEProm(void)
 *
 *     Function: Load FLASH Image (SplashScreen) to EEProm
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 /*
#if (LOAD_EXT_EEPROM_DEFAULT == 1)

void copyDataFromFlashToEEProm(void) 
{
  uint8_t ui8_Data;
  for (uint16_t ui16_Loop = 0; ui16_Loop <= sizeof(myBitmap) - 1; ui16_Loop++) 
  {
    ui8_Data = pgm_read_byte(&myBitmap[ui16_Loop]);
    updateEEPROM(EXT_EEPROM_SYSTEM_ADDRESS_LOGO + ui16_Loop, ui8_Data);
  }
}

#endif
*/
/************************************************************************************************
 *    EOF EEPROM functions
 ************************************************************************************************/




/************************************************************************************************
 *    BOF Story Game functions
 *
 *     Function: Functions for Story Game via the OLED Display
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

/************************************************************************************************
 *    void storyGameClear(void)
 *
 *     Function: Clear the story game gamble memory
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
/*
void storyGameClear(void)
{
  for(uint8_t _ui8_Index = 0; _ui8_Index < _ui8_NumOfGambleImages; _ui8_Index++) // clear game array
  { 
    _ui8ary_GambledImages[_ui8_Index] = 0xFF; // clear
  }
}
*/

/************************************************************************************************
 *    void drawCubeFromEEPROM(int8_t ui8_PosX, int8_t ui16_PosY, uint8_t ui8_CubeImageNumber) 
 *
 *     Function: Story Game Gamble (Play start)
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void drawCubeFromEEPROM(int8_t _ui8_PosX, int8_t _ui16_PosY, uint8_t _ui8_CubeImageNumber)
{
 // uint8_t _ui8_ScreenBufferIndex = 0;

  uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_GAME_IMAGES + (_ui8_CubeImageNumber * 128);   // start address + (_ui8_CubeImageNumber * FileSizeOfCubeImages) if the start address of file
  uint8_t __ui8ary_ScreenBuffer[128];
  
  for (uint8_t __ui8_ScreenBufferIndex = 0; __ui8_ScreenBufferIndex <= 128-1; __ui8_ScreenBufferIndex++)   // write line by line
  {
    __ui8ary_ScreenBuffer[__ui8_ScreenBufferIndex] = readEEPROM(__ui16_EEPROMAddress);
    __ui16_EEPROMAddress++;
  }
  display.drawBitmap(_ui8_PosX, _ui16_PosY, __ui8ary_ScreenBuffer, 32, 32, WHITE);
}


/************************************************************************************************
 *    void storyGameGamble(void)
 *
 *     Function: Story Game Gamble (Play start)
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void storyGameGamble(void)
{
  randomSeed(millis());
  uint8_t __ui8_AlreadyDrawn = 0;
//  uint8_t __ui8_XPos = 0, __ui8_YPos = 0;
  uint8_t __ui8ary_GambledImages[NUMBEROFCUBES];

//  storyGameClear(); // begin new game clear game memory

  for(uint8_t __ui8_Index = 0; __ui8_Index < NUMBEROFCUBES; __ui8_Index++) // clear game array
  { 
    __ui8ary_GambledImages[__ui8_Index] = 0xFF; // clear
  }

  for(uint8_t __ui8_IndexA = 0; __ui8_IndexA < NUMBEROFCUBES; __ui8_IndexA++)                             // check game array up to unique values
  {
    __ui8_AlreadyDrawn = 1;
    while (__ui8_AlreadyDrawn)
    {
      __ui8ary_GambledImages[__ui8_IndexA] = random(1, NUMBEROFIMAGES);                                             // RND from 1 to NUMBEROFIMAGES
      __ui8_AlreadyDrawn = 0;

      for(uint8_t __ui8_IndexB = 0; __ui8_IndexB < __ui8_IndexA; __ui8_IndexB++)                                    // scan the game memory of already drawn numbers 
      {
        if (__ui8ary_GambledImages[__ui8_IndexA] == __ui8ary_GambledImages[__ui8_IndexB]) __ui8_AlreadyDrawn = 1;     // if not a new number
      }
    }
  }
  
  display.clearDisplay();
  
//  display.setFont();
//  display.setTextSize(1);
//  display.setTextColor(SSD1306_WHITE);
//  display.setCursor(10, 10);
//  display.println("CURIOSITY GAME");
/*
  for(uint8_t __ui8_Index = 0; __ui8_Index < NUMBEROFCUBES; __ui8_Index++)        // output game array up to display values
  {
    Serial.print("cube: "); Serial.print(__ui8ary_GambledImages[__ui8_Index]); Serial.print(" : "); Serial.print(__ui8_XPos); Serial.print(" : "); Serial.println(__ui8_YPos);
    drawCubeFromEEPROM(__ui8_XPos, __ui8_YPos, __ui8ary_GambledImages[__ui8_Index]);        // get image from eeprom
    if (__ui8_XPos < 96) {__ui8_XPos += 32;} else {__ui8_XPos = 0; __ui8_YPos += 32;}       // shift pos to the next col or set to the first col and the next line
    delay(50);
  }
  */
  drawCubeFromEEPROM(0, 0, __ui8ary_GambledImages[0]);   drawCubeFromEEPROM(32, 0, __ui8ary_GambledImages[1]);   drawCubeFromEEPROM(64, 0, __ui8ary_GambledImages[2]);   drawCubeFromEEPROM(96, 0, __ui8ary_GambledImages[3]);
  drawCubeFromEEPROM(0, 32, __ui8ary_GambledImages[4]);   drawCubeFromEEPROM(32, 32, __ui8ary_GambledImages[5]);   drawCubeFromEEPROM(64, 32, __ui8ary_GambledImages[6]);   drawCubeFromEEPROM(96, 32, __ui8ary_GambledImages[7]);
  display.display();
}


/************************************************************************************************
 *    EOF Story Game functions
 ************************************************************************************************/




/************************************************************************************************
 *    BOF Flashlight functions
 *
 *     Function: Functions for flashlight via the Neopixel LEDs
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

/************************************************************************************************
 *    void initialFlashlight(void)
 *
 *     Function: Initialize the flashlight
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void initialFlashlight(void)
{          
  tds_Flashlight = {WHITE_LIGHT, WHITE_LIGHT, 0, OFF, OFF};   // the flashlight is switched off {color, previous color, brightness, mode, state, powerlimit}
}


/************************************************************************************************
 *    void setupFlashlight ( uint8_t _ui8_Color, uint8_t _ui8_Brightness, uint8_t _ui8_Mode)
 *
 *     Function: Setup the flashlight parameter for flashlight functions
 *
 *    Handovers: uint8_t _ui8_Color       => 0 = WHITE_LIGHT | 1 = RED_LIGHT | 2 = GREEN_LIGHT
 *               uint8_t _ui8_Brightness  => 0 - 50 if _ui8_Brightness > 50 then set to 50 (eye protection and heat generation!!!)
 *               uint8_t _ui8_Mode        => 0 = NORMAL_OFF | 1 = NORMAL_ON | 2 = FLASH | 3 = STROBE | 4 = SOS
 *               
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void setupFlashlight ( uint8_t _ui8_Color, uint8_t _ui8_Brightness, uint8_t _ui8_Mode)
{
  if (tds_Flashlight.ui8_Color != _ui8_Color)   // The color has changed
    tds_Flashlight.ui8_Color = _ui8_Color;      // Set the user color

  if (tds_Flashlight.ui8_Brightness != _ui8_Brightness)   // The brightness has changed
  {
    if (_ui8_Brightness < BRIGHTNESS) {tds_Flashlight.ui8_Brightness = _ui8_Brightness;} else {tds_Flashlight.ui8_Brightness = BRIGHTNESS;}     // Set the new user brightness or max brightness
  }
  
  if (tds_Flashlight.ui8_Mode != _ui8_Mode)   // The mode has changed
  {
    tds_Flashlight.ui8_Mode = _ui8_Mode;      // Update the mode of flashlight

    switch(tds_Flashlight.ui8_Mode)           // Save old user color and set the color for special function
    {
      case FLASH:   {tds_Flashlight.ui8_PreviousColor = tds_Flashlight.ui8_Color; tds_Flashlight.ui8_Color = RED_LIGHT;}   break;   // Save old user color and set red as color 
      case STROBE:  {tds_Flashlight.ui8_PreviousColor = tds_Flashlight.ui8_Color; tds_Flashlight.ui8_Color = RED_LIGHT;}   break;   // Save old user color and set red as color 
      case SOS:     {tds_Flashlight.ui8_PreviousColor = tds_Flashlight.ui8_Color; tds_Flashlight.ui8_Color = WHITE_LIGHT;} break;   // Save old user color and set white as color 
    }
  }
}


/************************************************************************************************
 *    void runnigFlashlight (void)
 *
 *     Function: Running flashlight function is the main function for task manager
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void runnigFlashlight (void)
{
  switch(tds_Flashlight.ui8_Mode)
  {
    case NORMAL_OFF: switchFlashlight(OFF); break;
    case NORMAL_ON:  switchFlashlight(ON);  break;
    case FLASH:      flashFlashlight();     break;    // 1 sec flashing
    case STROBE:     strobeFlashlight();    break;    // 3 100ms strobe every 1 sec
    case SOS:        sosFlashlight();       break;
    default:         switchFlashlight(OFF);
  }
}


/************************************************************************************************
 *    void switchFlashlight (uint8_t _ui8_State)
 *
 *     Function: Turn on/off the flashlight taking into account the parameters and system voltage.
 *
 *    Handovers: uint8_t _ui8_State => 0 = OFF | 1 = ON
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void switchFlashlight (uint8_t _ui8_State)
{
  uint8_t ui8_PowerLimitBrightness, ui8_LEDCount;

  if (tds_Flashlight.ui8_State != _ui8_State)                                               // We have a change of the status of the flashlight 
  {
    tds_Flashlight.ui8_State = _ui8_State;                                                  // update flashlight state
    if (tds_Flashlight.ui8_State)                                                           // if flashlight ON then
    {
      switch(ui8_SystemPowerLimit)
      {
         case 0: {ui8_PowerLimitBrightness = 255; ui8_LEDCount = COUNT_OF_LED;} break;      // full brightness
         case 1: {ui8_PowerLimitBrightness = 192; ui8_LEDCount = COUNT_OF_LED;} break;      // 75% brightness
         case 2: {ui8_PowerLimitBrightness = 128; ui8_LEDCount = 2;} break;                 // 50% brightness
         case 3: {ui8_PowerLimitBrightness =  64; ui8_LEDCount = 1;} break;                 // 25% brightness
         case 4: {ui8_PowerLimitBrightness =   0; ui8_LEDCount = 0;} break;                 //  0% brightness power off
        default: {ui8_PowerLimitBrightness = 255; ui8_LEDCount = COUNT_OF_LED;} break;      // full brightness
      }
    
      for (uint8_t ui8_Loop = 0; ui8_Loop < ui8_LEDCount; ui8_Loop++)
      {
        switch(tds_Flashlight.ui8_Color)
        {
          case WHITE_LIGHT: NeoPixelStrip.setPixelColor(ui8_Loop, 0x00, 0x00, 0x00, ui8_PowerLimitBrightness);  break;
          case RED_LIGHT:   NeoPixelStrip.setPixelColor(ui8_Loop, ui8_PowerLimitBrightness, 0x00, 0x00, 0x00);  break;
          case GREEN_LIGHT: NeoPixelStrip.setPixelColor(ui8_Loop, 0x00, ui8_PowerLimitBrightness, 0x00, 0x00);  break;
          default:          NeoPixelStrip.setPixelColor(ui8_Loop, 0x00, 0x00, 0x00, ui8_PowerLimitBrightness);  break;  // default is WHITE
        }
      }
    } 
    else                                                                        // flashlight is OFF
    {
      for (uint8_t ui8_Loop = 0; ui8_Loop <= COUNT_OF_LED; ui8_Loop++)
        NeoPixelStrip.setPixelColor(ui8_Loop, 0x00, 0x00, 0x00, 0x00);
    }
    NeoPixelStrip.setBrightness(tds_Flashlight.ui8_Brightness);
    NeoPixelStrip.show();
  }
}


/************************************************************************************************
 *    void flashFlashlight (void)
 *
 *     Function: Flashing light as an alert sign on the flashlight. 
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void flashFlashlight (void)
{ 
  if (tds_Flashlight.ui8_State) switchFlashlight(OFF); else switchFlashlight(ON);
  delay(500);
}


/************************************************************************************************
 *    void strobeFlashlight (void)
 *
 *     Function: Airplane strobe as a position sign on the flashlight. 
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void strobeFlashlight (void)
{ 
  for (uint8_t ui8_Loop = 0; ui8_Loop < 3; ui8_Loop++)
  {
    switchFlashlight(ON);
    delay(50);
    switchFlashlight(OFF);
    delay(100);
  }
  delay(900);
}


/************************************************************************************************
 *    BOF Morse functions
 *
 *     Function: Functions for outputting Morse code via the Neopixel LEDs
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void dot (void)  {switchFlashlight(ON); delay(MORSE_DOT);  switchFlashlight(OFF); delay(MORSE_SIGN_SPACE);}   // the dot this code make the led on for 150 than off for 150
void dash (void) {switchFlashlight(ON); delay(MORSE_DASH); switchFlashlight(OFF); delay(MORSE_SIGN_SPACE);}   // the dash this code make the led on for 450 than off for 150
void charSpace (void) {delay(MORSE_CHAR_SPACE);}                                                              // space between letters
void wordSpace (void) {delay(MORSE_WORD_SPACE);}                                                              // space between words

void morse_S (void) {dot(); dot(); dot(); charSpace();}                                                       // send char S
void morse_O (void) {dash(); dash(); dash(); charSpace();}                                                    // send char O

void sosFlashlight(void)                                                                                      // send fix text "SOS" morse main function for flashlight
{
  morse_S(); morse_O(); morse_S(); wordSpace();
}

/************************************************************************************************
 *    EOF Morse functions
 ************************************************************************************************/

/************************************************************************************************
 *    EOF Flashlight functions
 ************************************************************************************************/




/************************************************************************************************
 *    BOF Taskmanager functions
 *
 *     Function: Functions for flashlight via the Neopixel LEDs
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

 /************************************************************************************************
 *    void initialTaskManager (void)
 *
 *     Function: Initial the previous time for the task depending upon time.
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void initialTaskManager (void)
{
 uint32_t ui32_CurrentTime = millis(); // initial previous time for the tasks depending upon time.

 for(uint8_t ui8_Loop = 0; ui8_Loop < NUMBER_OF_TASKS - 1; ui8_Loop++)
 {
   ui32ary_PreviousTimeOfTask[NUMBER_OF_TASKS] = ui32_CurrentTime;
 }
}


 /************************************************************************************************
 *    void callTaskNow (uint8_t ui8_TaskNumber)
 *
 *     Function: Now go to the selected task. This is useful if you want to activate a part of the program 
 *               that was not activated before, for example the display will be updated immediately.
 *
 *    Handovers: uint8_t ui8_TaskNumber = 0 - (NUMBER_OF_TASKS - 1)
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/

void callTaskNow (uint8_t _ui8_TaskNumber)
{
 (*functptr[_ui8_TaskNumber])();
}


/************************************************************************************************
 *    void taskManager (void)
 *
 *     Function: this function handles the individual task calls, i.e. the timed call of the 
 *               corresponding task functions of the individual applications.
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void taskManager (void)
{
  uint32_t ui32_CurrentTime = millis();
  
  for (uint8_t ui8_TaskNumber = 0; ui8_TaskNumber < NUMBER_OF_TASKS; ui8_TaskNumber++)
  { 
    // its time for task x ?                                       
    if (ui32_CurrentTime - ui32ary_PreviousTimeOfTask[ui8_TaskNumber] > ui32ary_IntervalTimeOfTask[ui8_TaskNumber]) {
      if (ui8ary_TaskState[ui8_TaskNumber]) (*functptr[ui8_TaskNumber])();                                                // If the addressed task is used, invoke the task
      ui32ary_PreviousTimeOfTask[ui8_TaskNumber] = ui32_CurrentTime;                                                      // Update previous time for the next tasks depending upon time.
    }
  }
}


/************************************************************************************************
 *    void taskStateWrite (uint8_t ui8_Task, uint8_t ui8_State)
 *
 *     Function: this function handles the individual task calls, i.e. the timed call of the 
 *               corresponding task functions of the individual applications.
 *
 *    Handovers: uint8_t ui8_Task  = Number of task => from 0 to NUMBER_OF_TASKS - 1
 *               uint8_t ui8_State = 0 = is not used | 1 = is used 
 *               
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void taskStateWrite (uint8_t _ui8_Task, uint8_t _ui8_State)
{
  ui8ary_TaskState[_ui8_Task] = _ui8_State;
}


/************************************************************************************************
 *    void task_0_Curiosity_Heartbeat (void)
 *
 *     Function: this function display on heartbeat LED () thats the arduino its stil running.
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void task_0_Curiosity_Heartbeat (void)
{
  ui8_Heartbeat_State = !ui8_Heartbeat_State;
  digitalWrite(HEARDBEAT_LED_OF_ARDUINO, ui8_Heartbeat_State);
}


/************************************************************************************************
 *    void task_1_TerminalServer (void)
 *
 *     Function: This task function checks the incoming serial data and triggers the corresponding processes.
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void task_1_TerminalServer (void)
{
  if (Serial.available())
  {
    char __chr_TerminalServerInput;
    
    switch(ui8_SystemMode)
    {
      case RUN: {
                  __chr_TerminalServerInput = Serial.read();                                                                                      // read the serial input

                  if (__chr_TerminalServerInput == 'p' || __chr_TerminalServerInput == 'P')                                    // set system to program mode
                  {
                    Serial.println(F("SYSEM MODE IS NOW PROGRAMMING !"));
                    ui8_SystemMode &= ~RUN; 
                    ui8_SystemMode |= PROG;
                    ui8_LEDSystemMode &= ~LED_RUN;
                  }           
/*
                  if (__chr_TerminalServerInput == 'l' || __chr_TerminalServerInput == 'L')                                 // set active LED show
                  {
                    uint8_t __ui8_ShowNumber = Serial.parseInt();   // load show number
                    if (__ui8_ShowNumber >= MAX_OF_SHOWS){Serial.print(F("ERR: Incorrect show number. Possible is 0 - ")); Serial.println(MAX_OF_SHOWS - 1); return;}
                    ui8_LEDSystemMode &= ~LED_RUN; // stop led show
                    ui8_LEDSystemMode &= ~ACTIVE_LED_SHOW_BIT_MASK; // clear LED show number bits on ui8_LEDSystemMode register
                    ui8_LEDSystemMode |= (__ui8_ShowNumber & 0x03) << 4;
                    loadLedShow(__ui8_ShowNumber);
                    //getNextLedShowFrame(__ui8_ShowNumber);
                  //  ui8_FrameLoop = 0;              // reset global frame loop
                    //__ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_SHOW + FRAME_DATA_OFFSET + (_ui8_ShowNumber * LED_SHOW_BLOCK_SIZE);      // address of first byte of the show
                    ui8_FrameLoop = ui8_EepromProgramData[FRAMES] + 1;
                    ui8_LEDSystemMode |= LED_RUN; // restart led show
                    Serial.print("Active Show : "); Serial.println((ui8_LEDSystemMode & ACTIVE_LED_SHOW_BIT_MASK) >> 4);
                    Serial.println(); Serial.print("ui8_SystemMode : "); Serial.println(ui8_SystemMode);
                  }

                  if (__chr_TerminalServerInput == 't' || __chr_TerminalServerInput == 'T')                                    // set active user text number
                  {
                    uint8_t __ui8_UserTextNumber = Serial.parseInt();
                    uint8_t __ui8_Fault = 0;
                    
                    if (__ui8_UserTextNumber > MAX_NUMBER_OF_USER_TEXTS-1)
                    {
                      Serial.print(F("ERR: The text number maximum is: ")); Serial.println(MAX_NUMBER_OF_USER_TEXTS);
                      __ui8_Fault = 1;
                    }

                    if (__ui8_Fault == 0)
                    {
                      ui8_ShowTextNumber = __ui8_UserTextNumber;
                      Serial.print(F("The new text number is: ")); Serial.println(ui8_ShowTextNumber);
                      // ShowUserText(ui8_ShowTextNumber);
//                      str_UserText = readUserStringFromEEPROM(ui8_ShowTextNumber, ui8_Animation, ui8_FontSize);
//                      Serial.print(F("str_UserText: ")); Serial.println(str_UserText); Serial.println();
                    }
                  }
 */                 
                  if (__chr_TerminalServerInput == '?')                                                                         // User help in programming mode
                  {
                    Serial.println();
                    Serial.println(F("P = Set system to programming mode!")); Serial.println();
 //                   Serial.println(F("L = L followed by a one-digit number selects the LED show to be running."));
 //                   Serial.println(F("T = T followed by a one-digit number selects the text to be displayed."));
//                    Serial.println();
                  }           

      } break;
      case PROG: {
                  __chr_TerminalServerInput = Serial.read();                                                                 // get single char command
                  if (__chr_TerminalServerInput == 't' || __chr_TerminalServerInput == 'T')                                    // program the user text to ram
                  {
                    uint8_t __ui8_UserTextNumber = Serial.parseInt();
                    uint8_t __ui8_Animation  = Serial.parseInt();
                    uint8_t __ui8_FontSize  = Serial.parseInt();
                     String __str_RxUserString = Serial.readString();
                    uint8_t __ui8_StrLength = __str_RxUserString.length();
                     char __chrary_UserTextData[__str_RxUserString.length() + 1];
                     __str_RxUserString.toCharArray(__chrary_UserTextData, __str_RxUserString.length() + 1);
                 

/*
  String myString = "Arduino";
  byte buffer[myString.length() + 1];

  myString.toCharArray(buffer, myString.length() + 1);

  for (int i = 0; i < myString.length() + 1; i++)
    Serial.println(buffer[i], HEX);
 */





                    uint8_t __ui8_Fault = 0;

                    if (__ui8_UserTextNumber > MAX_NUMBER_OF_USER_TEXTS)
                    {
                      Serial.print(F("The text number is maximum: ")); Serial.println(MAX_NUMBER_OF_USER_TEXTS);
                      __ui8_Fault = 1;
                    }
                   
                    if (__ui8_Animation > MAX_NUMBER_OF_ANIMATION)
                    {
                      Serial.print(F("The animation number is maximum: ")); Serial.println(MAX_NUMBER_OF_ANIMATION);
                      __ui8_Fault |= 2;
                    }

                    if (__ui8_FontSize > MAX_SIZE_OF_FONT)
                    {
                      Serial.print(F("The maximum size of font is: ")); Serial.println(MAX_SIZE_OF_FONT);
                      __ui8_Fault |= 4;
                    }
                    
                    if (__ui8_StrLength > MAX_LENGTH_PER_TEXT)
                    {
                      Serial.print(F("The maximum text length is: ")); Serial.println(MAX_LENGTH_PER_TEXT);
                      __ui8_Fault |= 8;
                    }
                    
                    if (__ui8_Fault == 0)
                    {
                      Serial.println(F("Programming user text: ")); 
                      Serial.print(F("Number:    ")); Serial.println(__ui8_UserTextNumber);
                      Serial.print(F("Animation: ")); Serial.println(__ui8_Animation);  
                      Serial.print(F("Font size: ")); Serial.println(__ui8_FontSize);
                      Serial.print(F("Length:    ")); Serial.println(__ui8_StrLength);
                      Serial.print(F("Text:      ")); Serial.println(__chrary_UserTextData);
                      Serial.println();
//                      updateUserStringToEEPROM(__ui8_UserTextNumber, __ui8_Animation, __ui8_FontSize, __str_RxUserString);

//void updateUserStringToEEPROM(uint8_t &_ui8_TextNumber, uint8_t &_ui8_Animation, uint8_t &_ui8_FontSize, String &_str_StrToWrite)
//{
    uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_TEXT + (__ui8_UserTextNumber * (4 + MAX_LENGTH_PER_TEXT));               // address of first byte of the text header 1 Byte Animation + 1 Byte Font Size + 1 Byte Length + max 29 Byte char
//    uint8_t __ui8_TextLength = __str_RxUserString.length();
  
    updateEEPROM(__ui16_EEPROMAddress, __ui8_Animation);                                  // animations type
    __ui16_EEPROMAddress++;                                                               // increment directory address to the next 
  
    updateEEPROM(__ui16_EEPROMAddress, __ui8_FontSize);                                   // selected font size
    __ui16_EEPROMAddress++;                                                               // increment directory address to the next 
  
    updateEEPROM(__ui16_EEPROMAddress, __ui8_StrLength);                                 // the real length of user text
    __ui16_EEPROMAddress++;                                                               // increment directory address to the next 
  
    updateEEPROM(__ui16_EEPROMAddress, 0);                                                // reserve for future use
    __ui16_EEPROMAddress++;                                                               // increment directory address to the next 
    
    for (uint8_t __ui8_CharPos = 0; __ui8_CharPos < __ui8_StrLength; __ui8_CharPos++)
    {
      updateEEPROM(__ui16_EEPROMAddress, __chrary_UserTextData[__ui8_CharPos]);
      __ui16_EEPROMAddress++;                                                           // increment directory address to the next 
    }



                      
                      delay(200);
                      //ShowUserText(__ui8_UserTextNumber);
                      ui8_ShowTextNumber = __ui8_UserTextNumber;
//                      String str_UserText = readUserStringFromEEPROM(ui8_ShowTextNumber, __ui8_Animation, __ui8_FontSize);
//                      Serial.print(F("update str_UserText: ")); Serial.println(str_UserText); Serial.println();
                    }
     
                  }      
                       
                  if (__chr_TerminalServerInput == 'c' || __chr_TerminalServerInput == 'C')                                    // clear the user text to ram
                  {
                    uint8_t __ui8_DataType = Serial.parseInt();
                    uint8_t __ui8_FileNumber  = Serial.parseInt();
                    uint8_t __ui8_Fault = 0;

                    if (__ui8_DataType > 0)
                    {
                      Serial.print(F("The data type number is maximum: ")); Serial.println(0);
                      __ui8_Fault = 1;
                    }

                    switch(__ui8_DataType)
                    {
                      case 0: {
                                if (__ui8_FileNumber > MAX_NUMBER_OF_USER_TEXTS)
                                {
                                  Serial.print(F("The text number is maximum: ")); Serial.println(MAX_NUMBER_OF_USER_TEXTS - 1);
                                  __ui8_Fault |= 2;
                                }
                                
                                if (__ui8_Fault == 0)
                                {
                                  Serial.print(F("Clear user text number: ")); Serial.print(__ui8_FileNumber);
                                  clearUserStringToEEPROM(__ui8_FileNumber);
                                  Serial.println(F(" done"));
                                  delay(200);
                                }
                      } break;
                   }
                  }

                  if (__chr_TerminalServerInput == 'd' || __chr_TerminalServerInput == 'D')                                                       // delet the selected show
                  {
                    uint8_t __ui8_ShowNumber = Serial.parseInt();
                    if (__ui8_ShowNumber < MAX_OF_SHOWS)
                    {
                      Serial.print(F("ClearShow : ")); Serial.print(__ui8_ShowNumber);
                      clearShow(__ui8_ShowNumber);
                      delay(500);
                      Serial.println(F(" done."));
                    }
                    else Serial.print(F("ERR: Incorrect show number possible is 0 - ")); Serial.println(MAX_OF_SHOWS - 1);
                  }

                  if (__chr_TerminalServerInput == 'l' || __chr_TerminalServerInput == 'L')                                                       // set show program data
                  {
                    uint8_t __ui8_ShowNumber = Serial.parseInt();   // get show number
                    if (__ui8_ShowNumber >= MAX_OF_SHOWS) {Serial.print(F("ERR: Incorrect show number. Possible is 0 - ")); Serial.println(MAX_OF_SHOWS - 1); return;}
                    uint8_t __ui8_Frames = Serial.parseInt();                                                                                     // get frames
                    if (__ui8_Frames >= MAX_FRAMES_PER_SHOW) {Serial.print(F("ERR: Incorrect frame number. Possible is 0 - ")); Serial.println(MAX_FRAMES_PER_SHOW - 1); return;}
                    uint16_t __ui16_ShowPause = Serial.parseInt();                                                                                // get show replay pause
               //     uint8_t __ui8_Leds = Serial.parseInt()-1;          // get count of show Leds
               //     if (__ui8_Leds > COUNT_OF_LED) {Serial.print(F("Err: Incorrect count of LEDs. Possible 0 - ")); Serial.println(COUNT_OF_LED - 1); return;}
                
                    
                    updateLedShow (__ui8_ShowNumber, __ui8_Frames, __ui16_ShowPause, COUNT_OF_LED);                                               // __ui8_Leds is fix on curiosity V1.xy
                    delay(200);                                                                                                                   // wait for EEPROM com stabilized
                    loadLedShow(__ui8_ShowNumber);                                                                                                // get show data from EEPROM
              //      getAvailableLedShows();
                    Serial.println(F("PROGRAM SHOW DATA IS DONE")); 
                      Serial.print(F("             Show : ")); Serial.println(__ui8_ShowNumber);
                      Serial.print(F("           Frames : ")); Serial.println(ui8_EepromProgramData[FRAMES]);
                      Serial.print(F("Show replay pause : ")); Serial.println((ui8_EepromProgramData[SHOW_DELAY]<<8)|(ui8_EepromProgramData[SHOW_DELAY+1]&0xFF));
                      Serial.print(F("    Count of LEDs : ")); Serial.println(ui8_EepromProgramData[FRAME_LED_COUNT]);
                      Serial.println(); 
                  }

                  if (__chr_TerminalServerInput == 'f' || __chr_TerminalServerInput == 'F')                                                       // set show program pause
                  {
                    uint8_t __ui8_PosCount;
                    for ( __ui8_PosCount = 0; __ui8_PosCount < 18; __ui8_PosCount++)
                    {
                      ui8_LedShowFrameBuffer[__ui8_PosCount] = Serial.parseInt();
                      #ifdef DEBUG_SERIAL_OUTPUT
                        Serial.print(F("__ui8_PosCount : ")); Serial.print(__ui8_PosCount);
                        Serial.print(F(" : ")); Serial.println(ui8_LedShowFrameBuffer[__ui8_PosCount]);
                      #endif
                    }
                    uint16_t __ui16_Buffer = Serial.parseInt();
                    #ifdef DEBUG_SERIAL_OUTPUT
                      Serial.print(F("__ui8_PosCount delay : ")); Serial.print(__ui8_PosCount);
                      Serial.print(F(" : ")); Serial.println(__ui16_Buffer);
                    #endif
                    
                    ui8_LedShowFrameBuffer[__ui8_PosCount] = __ui16_Buffer >> 8;
                    #ifdef DEBUG_SERIAL_OUTPUT
                      Serial.print(F("__ui8_PosCount : ")); Serial.print(__ui8_PosCount);
                      Serial.print(F(" : ")); Serial.println(ui8_LedShowFrameBuffer[__ui8_PosCount]);
                    #endif
                    
                    __ui8_PosCount++;
                    ui8_LedShowFrameBuffer[__ui8_PosCount] = __ui16_Buffer & 0x00FF;
                    #ifdef DEBUG_SERIAL_OUTPUT
                      Serial.print(F("__ui8_PosCount : ")); Serial.print(__ui8_PosCount);
                      Serial.print(F(" : ")); Serial.println(ui8_LedShowFrameBuffer[__ui8_PosCount]);
                    #endif
            
                    // show on leds
                    uint8_t __ui8_EepromLedColor[COUNT_OF_LED_COLORS];                    // Contains R, G, B, W values
            
                    __ui8_PosCount = 2; // first led and chip
                    for(uint8_t __ui8_LEDLoop = 0; __ui8_LEDLoop < ui8_EepromProgramData[FRAME_LED_COUNT]; __ui8_LEDLoop++) 
                    {
                      for(uint8_t __ui8_LEDColorLoop = 0; __ui8_LEDColorLoop < COUNT_OF_LED_COLORS; __ui8_LEDColorLoop++)
                      {
//                        Serial.print(F("EEPROM Address : ")); Serial.print(__ui16_EEPROMAddress);
                        __ui8_EepromLedColor[__ui8_LEDColorLoop] = ui8_LedShowFrameBuffer[__ui8_PosCount]; // Get RGBW data for one led
                        #ifdef DEBUG_SERIAL_OUTPUT
                          Serial.print(__ui8_EepromLedColor[__ui8_LEDColorLoop]); Serial.print(",");
                        #endif
                        __ui8_PosCount++;
                      }
                      NeoPixelStrip.setPixelColor(__ui8_LEDLoop, NeoPixelStrip.Color(__ui8_EepromLedColor[RED_LED], __ui8_EepromLedColor[GREEN_LED], __ui8_EepromLedColor[BLUE_LED], __ui8_EepromLedColor[WHITE_LED]));   // Set one led with RGBW value
                    }
                    NeoPixelStrip.show();  // Display on the strip
                    ui8_LEDSystemMode |= PROGRAM_NEW_FRAME_DATA; // set new frame data available
                    #ifdef DEBUG_SERIAL_OUTPUT
                      Serial.println(); Serial.print("ui8_LEDSystemMode : "); Serial.println(ui8_LEDSystemMode);
                    #endif;
                    Serial.println(); Serial.print("Show frame is set in to the RAM. "); Serial.println();
                  }


                  if ((ui8_LEDSystemMode & PROGRAM_NEW_FRAME_DATA) == PROGRAM_NEW_FRAME_DATA)
                  {
                    if (__chr_TerminalServerInput == 'p' || __chr_TerminalServerInput == 'P')                                                       // set show program pause
                    {
                      uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_SHOW + FRAME_DATA_OFFSET + (ui8_LedShowFrameBuffer[0] * LED_SHOW_BLOCK_SIZE) + (ui8_LedShowFrameBuffer[1] * FRAME_DATA_SIZE);  // address of first byte of the show
                      #ifdef DEBUG_SERIAL_OUTPUT
                        Serial.print(F("__ui16_EEPROMAddress : ")); Serial.println(__ui16_EEPROMAddress);
                      #endif;
                      // copy LED color datas from RAM to the EEPROM
                      uint8_t __ui8_PosCount = 2; // first led and chip
                      for(uint8_t __ui8_LedLoop = 0; __ui8_LedLoop < COUNT_OF_LED; __ui8_LedLoop++) 
                      { 
                        #ifdef DEBUG_SERIAL_OUTPUT
                          Serial.print(F("__ui8_LedLoop : ")); Serial.println(__ui8_LedLoop);
                        #endif;
                        for(uint8_t __ui8_LEDColorLoop = 0; __ui8_LEDColorLoop < COUNT_OF_LED_COLORS; __ui8_LEDColorLoop++)
                        {
                          #ifdef DEBUG_SERIAL_OUTPUT
                            Serial.print(__ui16_EEPROMAddress); Serial.print(F(" : __ui8_LEDColorLoop : ")); Serial.print(__ui8_LEDColorLoop); Serial.print(F(" : __ui8_PosCount : ")); Serial.println(__ui8_PosCount);
                          #endif;
                          updateEEPROM(__ui16_EEPROMAddress, ui8_LedShowFrameBuffer[__ui8_PosCount]);       // Write eeprom with RGBW data for one led from RAM
                          __ui8_PosCount++;         // go to the next vallue from RAM
                          __ui16_EEPROMAddress++;
                        }
                      }
                      // copy frame delay data from RAM to the EEPROM
                      #ifdef DEBUG_SERIAL_OUTPUT
                        Serial.println(F("Frame delay high byte : ")); Serial.print(__ui16_EEPROMAddress); Serial.print(F(" : __ui8_PosCount : ")); Serial.print(__ui8_PosCount); Serial.print(F(" : HB F Delay : ")); Serial.println(ui8_LedShowFrameBuffer[__ui8_PosCount]);
                      #endif;
                      updateEEPROM(__ui16_EEPROMAddress, ui8_LedShowFrameBuffer[__ui8_PosCount]);       // Write eeprom with frame delay data high byte from RAM
            
                      __ui8_PosCount++;         // go to the next vallue from RAM
                      __ui16_EEPROMAddress++;
                      #ifdef DEBUG_SERIAL_OUTPUT
                        Serial.println(F("Frame delay high byte : ")); Serial.print(__ui16_EEPROMAddress); Serial.print(F(" : __ui8_PosCount : ")); Serial.print(__ui8_PosCount); Serial.print(F(" : LB F Delay : ")); Serial.println(ui8_LedShowFrameBuffer[__ui8_PosCount]);
                      #endif;
                      updateEEPROM(__ui16_EEPROMAddress, ui8_LedShowFrameBuffer[__ui8_PosCount]);       // Write eeprom with frame delay data low byte from RAM
            
                      #ifdef DEBUG_SERIAL_OUTPUT
                        Serial.println(); Serial.print(F("EEPROM Address : ")); Serial.print(__ui16_EEPROMAddress); Serial.println(F(" copy to EEPROM done.")); Serial.println();
                      #endif;
                      ui8_SystemMode &= ~PROGRAM_NEW_FRAME_DATA;  // clear new frame data available
                      #ifdef DEBUG_SERIAL_OUTPUT
                        Serial.println(); Serial.print(F("ui8_SystemMode : ")); Serial.println(ui8_SystemMode);
                      #endif;
                      Serial.println(); Serial.println(F("LED show frame programming to EEPROM : done")); Serial.println();
                    }
                  }
/*
                  if (__chr_TerminalServerInput == 'm' || __chr_TerminalServerInput == 'M') // read out EEPROM as dump
                  {
                   // uint16_t __ui16_EEPROMAddress = 0x1000;
                    uint8_t __ui8_TempData;
                    Serial.println(F("EEPROM DUMP :"));
                    for (uint16_t __ui16_EEPROMAddress = 0x1000 - 1; __ui16_EEPROMAddress < 0x2310; __ui16_EEPROMAddress++)        // row loop 512kbit
//                    for (uint8_t _ui8_RowAddress = 16; _ui8_RowAddress < 64; _ui8_RowAddress++)        // row loop 128kbit
                    {
                      for (uint8_t _ui8_ColAddress = 0; _ui8_ColAddress < 16; _ui8_ColAddress++)   // col loop
                      {
                        __ui16_EEPROMAddress++;
                        //__ui16_EEPROMAddress = (_ui8_RowAddress << 4) | _ui8_ColAddress;
                        if (_ui8_ColAddress == 0)
                        {
                          Serial.print(__ui16_EEPROMAddress < 4096 ? "0" : ""); // print or not a leading zero
                          Serial.print(__ui16_EEPROMAddress <  256 ? "0" : ""); // print or not a leading zero
                          Serial.print(__ui16_EEPROMAddress <   16 ? "0" : ""); // print or not a leading zero
                          Serial.print(__ui16_EEPROMAddress, HEX); Serial.print(" : ");
                        }
                        __ui8_TempData = readEEPROM(__ui16_EEPROMAddress);
                        Serial.print(__ui8_TempData < 16 ? "0" : ""); // print or not a leading zero
                        Serial.print(__ui8_TempData, HEX); Serial.print(" ");
                        
                      }
                      Serial.println();
                    }
                  }
*/      
                            
                  if (__chr_TerminalServerInput == 'e' || __chr_TerminalServerInput == 'E')       // set system to program user text mode
                  {
                    Serial.println(F("SYSEM MODE IS NOW RUNNING !"));
                    ui8_SystemMode &= ~PROG; 
                    ui8_SystemMode |= RUN;
                    //ui8_SystemMode &= ~PROGRAM_LED_SHOW; 
           //         ui8_FrameLoop = ui8_EepromProgramData[FRAMES] + 1;
                    ui8_LEDSystemMode |= LED_RUN;
                  } 

                  if (__chr_TerminalServerInput == '?')                                                                           // User help in programming mode
                  {
                    Serial.println();
                    Serial.println(F("T = followed by a one-digit number selects the text to be programming."));
                    Serial.println(F("    followed by a one-digit number selects the animation for this text."));
                    Serial.println(F("    followed by a one-digit number selects the font size for this text."));
                    Serial.println(F("    followed by the text."));
                    Serial.println(F("    Example: T0 1 3 HALLO")); Serial.println();
                    
                    Serial.println(F("C = followed by a one-digit number selects the text to be cleared.")); Serial.println();
                    
                    Serial.println(F("L = followed by a one-digit number selects the LED show to be programming."));
                    Serial.println(F("    followed by a one-digit or two-digit number set count of frames of the animation. max. 32"));
                    Serial.println(F("    followed by a one-digit or max five digit number set the show pause."));
                    Serial.println(F("    Example: L0 4 2000 => Set show: 0 | Frames: 4 | Show repeat pause: 2 sec.")); Serial.println();
                    
                    Serial.println(F("F = followed by a one-digit number set the show."));
                    Serial.println(F("    followed by a one-digit or two-digit number set the frame of the animation. max. 32"));
                    Serial.println(F("    folloing 16 numbers by a one-digit or max 3 digits number represent the  RGBW values of the 4 leds.")); Serial.println();
                    Serial.println(F("    followed by a one-digit or max five digit number set the frame delay.")); Serial.println();
                    Serial.println(F("    Example: F0 0 255 0 0 0 ..... 500 => Set show: 0 | Frame: 0 | firt LED: R = 255, G = 0, B = 0, W = 0 | Frame delay: 500 ms")); Serial.println();

                    Serial.println(F("P = Save the frame in the eeprom.")); Serial.println();
                    
                    Serial.println(F("D = followed by a one-digit number selects the show to be delete.")); Serial.println();

//                    Serial.println(F("M = EEPROM memory HEX dump.")); Serial.println();

                    Serial.println(F("E = Exit from programming mode to the running mode!")); Serial.println();
                  }           
      } break;
    }
  }
}        
 
             /*     if (chr_TerminalServerInput == 'a' || chr_TerminalServerInput == 'A') {setupFlashlight ( WHITE, BRIGHTNESS, SOS);}            // flashligt set to the white sos mode
                  if (chr_TerminalServerInput == 'f' || chr_TerminalServerInput == 'F') {setupFlashlight ( WHITE, BRIGHTNESS, FLASH);}          // flashligt set to the red flash mode
                  if (chr_TerminalServerInput == 'o' || chr_TerminalServerInput == 'O') {setupFlashlight ( WHITE, BRIGHTNESS, NORMAL_OFF);}     // flashligt set to the switch off the flashlight
                  if (chr_TerminalServerInput == 's' || chr_TerminalServerInput == 'S') {setupFlashlight ( WHITE, BRIGHTNESS, STROBE);}         // flashligt set to the red airplane strobe mode
                  if (chr_TerminalServerInput == 'w' || chr_TerminalServerInput == 'W') {switchFlashlight(OFF); setupFlashlight ( WHITE_LED, BRIGHTNESS, NORMAL_ON);}      // flashligt set to the switch on the white flashlight
                  if (chr_TerminalServerInput == 'r' || chr_TerminalServerInput == 'R') {switchFlashlight(OFF); setupFlashlight ( RED_LED, BRIGHTNESS, NORMAL_ON);}        // flashligt set to the switch on the red flashlight
                  if (chr_TerminalServerInput == 'g' || chr_TerminalServerInput == 'G') {switchFlashlight(OFF); setupFlashlight ( GREEN_LED, BRIGHTNESS, NORMAL_ON);}      // flashligt set to the switch on the green flashlight
             *///     if (chr_TerminalServerInput == 'v' || chr_TerminalServerInput == 'V') {taskStateWrite(SYSTEM_VOLTAGE_ON_SCREEN, TASK_IS_USED); taskStateWrite(USER_TEXT_DISPLAY_TASK, TASK_ISNT_IN_USE); callTaskNow(SYSTEM_VOLTAGE_ON_SCREEN);}
             //     if (chr_TerminalServerInput == 'i' || chr_TerminalServerInput == 'I') {taskStateWrite(SYSTEM_VOLTAGE_ON_SCREEN, TASK_ISNT_IN_USE); taskStateWrite(USER_TEXT_DISPLAY_TASK, TASK_ISNT_IN_USE); drawSplashScreen();}
             //     if (chr_TerminalServerInput == 't' || chr_TerminalServerInput == 'T') {taskStateWrite(SYSTEM_VOLTAGE_ON_SCREEN, TASK_ISNT_IN_USE); taskStateWrite(USER_TEXT_DISPLAY_TASK, TASK_IS_USED); callTaskNow(USER_TEXT_DISPLAY_TASK);}
//                  if (chr_TerminalServerInput == 'p' || chr_TerminalServerInput == 'P') {ui8_SystemMode = PROG; programSerialUserText();}
            //    if (chr_TerminalServerInput == 'd' || chr_TerminalServerInput == 'D') {goToSleep();}                                          // shutdown the system
//                  if (chr_TerminalServerInput == 'c' || chr_TerminalServerInput == 'C') {storyGameGamble();}                                    // shutdown the system
            //      if (chr_TerminalServerInput == 'x' || chr_TerminalServerInput == 'X') {ui8_SystemPowerLimit = getSystemPowerLimit();}         // get the actual power limit of the system
/*
                  if (chr_TerminalServerInput == 'x' || chr_TerminalServerInput == 'X') {GetEEPROMDump();}

                  if (chr_TerminalServerInput == '0') {display.clearDisplay(); drawImageFromEEPROM(0); display.display();}
                  if (chr_TerminalServerInput == '1') {display.clearDisplay(); drawImageFromEEPROM(1);display.display();}
            
                } break; */
/*      case PROG : {
                    if (chr_TerminalServerInput == 't' || chr_TerminalServerInput == 'T') {ui8_SystemMode = PROG_TXT;}
                    if (chr_TerminalServerInput == '#') {ui8_SystemMode = RUN;}
                  } break;
      case PROG_TXT: { 
                       if (chr_TerminalServerInput == '#') {ui8_SystemMode = RUN;}
                       programSerialUserText(); 
                     } break;
 */


                        
 //  }
/*    
 *     
    if (chr_TerminalServerInput == '0') {ui8_SystemPowerLimit = 0;}  // Set the power limit on 03 for test of the system (no limit)
    if (chr_TerminalServerInput == '1') {ui8_SystemPowerLimit = 1;}  // Set the power limit on 1 for test of the system
    if (chr_TerminalServerInput == '2') {ui8_SystemPowerLimit = 2;}  // Set the power limit on 2 for test of the system
    if (chr_TerminalServerInput == '3') {ui8_SystemPowerLimit = 3;}  // Set the power limit on 3 for test of the system
    if (chr_TerminalServerInput == '4') {ui8_SystemPowerLimit = 4;}  // Set the power limit on 4 for test of the system (switch off neopixels)
*/
//    Serial.println (chr_TerminalServerInput);                                                                                     // print the latter saved in the input var
//  }
// }

/************************************************************************************************
 *    void task_2_Flashlight (void)
 *
 *     Function: This task function control  the flashlight, checks the status of the flashlight data. 
 *               The flashlight is updated based on this data.
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void task_2_Flashlight (void)
{
  runnigFlashlight();
}


/************************************************************************************************
 *    void task_3_SystemPowerLimit (void)
 *
 *     Function: This task function determines the system voltage, updates the global system register 
 *               SystemPowerLimit and adjusts the interval time for this task call accordingly. 
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void task_3_SystemPowerLimit (void)
{
/*  ui8_SystemPowerLimit = getSystemPowerLimit();         // Get the system power limit
  switch(ui8_SystemPowerLimit)
  {
     case 0: ui32ary_IntervalTimeOfTask[3] = INTERVAL_OF_TASK_3_300000; break;       // This task called every 5 min
     case 1: ui32ary_IntervalTimeOfTask[3] = INTERVAL_OF_TASK_3_150000; break;       // Have this task called more often  
     case 2: ui32ary_IntervalTimeOfTask[3] = INTERVAL_OF_TASK_3_75000;  break;       // Have this task called more often  
     case 3: ui32ary_IntervalTimeOfTask[3] = INTERVAL_OF_TASK_3_150000; break;       // Have this task called more often 
    default: ui32ary_IntervalTimeOfTask[3] = INTERVAL_OF_TASK_3_300000; break;       // This task called every 5 min 
  }*/
}

/************************************************************************************************
 *    void task_4_ShowVBAT (void)
 *
 *     Function: This task function checks the system voltage and updates the value on the display. 
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void task_4_ShowVBAT (void)
{
//  vbatDisplay();
}


/************************************************************************************************
 *    void task_5_ShowUserText (void)
 *
 *     Function: This task running the program for user text animation on the display. 
 *
 *    Handovers: void
 *       Return: void
 *
 *        Autor: Daniel Bossy v/o Rodeo | HB9EUB
 *     Revision: V1.00
 ************************************************************************************************/
 
void task_5_ShowUserText (void)
{
  Serial.println(F("task_5_ShowUserText"));
//  scrollText();
}


/************************************************************************************************
 *    BOF Taskmanager functions
 ************************************************************************************************/
 
/************************************************************************************************
 *    EOF CURIOSITY_BULA_FULL_APP_V1.01
 ************************************************************************************************/
