/************************************************************************************************
*  CURIOSITY_Test.ino
*
*  Target Board: ScienceScout CURIOSITY V1.07
*    Target MPU: ATmega328P (Arduino Nano)
*       Created: 07.2022
*        Author: Adrian Dummermuth v/o Calimero | HB9HNV
*      Revision: V1.10
*  Copyright by: HB9HNV (source is free for noncommercial and education project.)
*          Note: I assume no liability for errors, defects and malfunctions that occur during
*                the application of the codes.
*                This code is free and open for anybody to use at their own risk.
*      Function: Hardware testprogram
************************************************************************************************/

#include <avr/pgmspace.h>
#include <Wire.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiWire.h"

#include <Adafruit_GFX.h>
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>

/************************************************************************************************
 *  Public global function class build for Curiosity Hardware V1.07
 ************************************************************************************************/

//#define USE_SERIAL
#define SERIAL_ADDRESS 1000
#define LED_STRIP_PIN    9
#define COUNT_OF_LED     4
#define BRIGHTNESS      20

Adafruit_NeoPixel NeoPixelStrip = Adafruit_NeoPixel(COUNT_OF_LED, LED_STRIP_PIN, NEO_GRBW + NEO_KHZ800);
uint32_t colours[8];

#define OLED_ADDR   0x3C
SSD1306AsciiWire oled;

#define PIN_BUTTON_UP 14
#define PIN_BUTTON_DOWN 15
#define PIN_BUTTON_LEFT 16
#define PIN_BUTTON_RIGHT 17
#define PIN_BUTTON_ENTER 2


/************************************************************************************************
 *  Public global definitions for Curiosity Hardware V1.07
 ************************************************************************************************/

#define EEPROM_I2C_ADDRESS                         0x50 // EEPROM I2C Address
#define EXT_EEPROM_SYSTEM_ADDRESS_USER_IMAGE     0x1000 // EEPROM I2C Address of the first user image
#define FILESIZE                                   1024 // for simplification, in case of curiosity, the step size of the user images is always fixed at 1024 

/************************************************************************************************
 *  Public global variables for Curiosity Hardware V1.07
 ************************************************************************************************/

uint16_t ui16_FaultAddress;

/************************************************************************************************
*  void setup()
************************************************************************************************/
#define OFF 0
#define W 1
#define R 2
#define G 3
#define B 4
#define RW 5
#define GW 6
#define BW 7

uint32_t id = 0;
char buf[255];
void setup()
{ 
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_BUTTON_ENTER, INPUT);
  pinMode(PIN_BUTTON_UP, INPUT_PULLUP);
  pinMode(PIN_BUTTON_DOWN, INPUT_PULLUP);
  pinMode(PIN_BUTTON_LEFT, INPUT_PULLUP);
  pinMode(PIN_BUTTON_RIGHT, INPUT_PULLUP);

  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(9600);
  Serial.println(F("## Curiosity Hardware Test ##"));

  #ifdef USE_SERIAL
  EEPROM.begin();
  EEPROM.get(SERIAL_ADDRESS, id);
  while ((id == 0xffffffff) || (id == 0x00000000))
  {
    Serial.println(F("Enter serial number:"));
    Serial.setTimeout(2000);
    id = Serial.parseInt();
  }
  EEPROM.put(SERIAL_ADDRESS, id);
  sprintf(buf, "Device serial number: %lu", id);
  Serial.println(buf);
  #else
  Serial.println(F("No serial used"));
  #endif

  Serial.println(F("Setting up variables"));

  colours[0] = NeoPixelStrip.Color(0, 0, 0, 0);
  colours[1] = NeoPixelStrip.Color(0, 0, 0, 255);
  colours[2] = NeoPixelStrip.Color(255, 0, 0, 0);
  colours[3] = NeoPixelStrip.Color(0, 255, 0, 0);
  colours[4] = NeoPixelStrip.Color(0, 0, 255, 0);
  colours[5] = NeoPixelStrip.Color(255, 0, 0, 255);
  colours[6] = NeoPixelStrip.Color(0, 255, 0, 255);
  colours[7] = NeoPixelStrip.Color(0, 0, 255, 255);

  Serial.println("Start I2C");
  Wire.begin(); // Connect to I2C bus as master

  Serial.println("Testing LED");
  NeoPixelStrip.begin();
  NeoPixelStrip.show();

  for (uint8_t led_count = 0; led_count < COUNT_OF_LED; led_count++)
  {
    NeoPixelStrip.setPixelColor(led_count, colours[W]);
  }
  NeoPixelStrip.show();
  NeoPixelStrip.setBrightness(BRIGHTNESS);
  delay(2000);
  digitalWrite(LED_BUILTIN, LOW);
  for (uint8_t led_count = 0; led_count < COUNT_OF_LED; led_count++)
  {
    NeoPixelStrip.setPixelColor(led_count, colours[OFF]);
  }
  NeoPixelStrip.show();

  Serial.println(F("Testing external EEPROM"));
  uint8_t temp = 0;
  for (uint8_t addr = 0; addr < 10; addr++)
  {
    Serial.print(F("Checking address "));
    Serial.println(addr, HEX);
    writeEEPROM(addr, 0xAA);
    temp = readEEPROM(addr);

    if (temp != 0xAA)
    {
      break;
    }
  }

  if (temp == 0xAA)
  {
    Serial.println(F("EEPROM OK"));
    for (uint8_t led_count = 0; led_count < COUNT_OF_LED; led_count++)
    {
      NeoPixelStrip.setPixelColor(led_count, colours[G]);
    }
  }
  else
  {
    Serial.println(F("EEPROM test failed"));
    for (uint8_t led_count = 0; led_count < COUNT_OF_LED; led_count++)
    {
      NeoPixelStrip.setPixelColor(led_count, colours[R]);
    }
  }
  NeoPixelStrip.show();
  delay(2000);
  digitalWrite(LED_BUILTIN, HIGH);

  Serial.println(F("Turning off LED"));
  for (uint8_t led_count = 0; led_count < COUNT_OF_LED; led_count++)
  {
    NeoPixelStrip.setPixelColor(led_count, colours[OFF]);
  }
  NeoPixelStrip.show();

  Serial.println(F("Testing display"));

  oled.begin(&Adafruit128x64, OLED_ADDR);
  oled.setFont(System5x7);
  oled.clear();
  oled.print(F("Hello Curiosity!"));
}

uint16_t colour_led_0 = 0;
uint16_t colour_led_1 = 0;
uint16_t colour_led_2 = 0;
uint16_t colour_led_3 = 0;

uint8_t led = 0;
void loop()
{
  uint8_t enter = digitalRead(PIN_BUTTON_ENTER);
  uint8_t up = digitalRead(PIN_BUTTON_UP);
  uint8_t down = digitalRead(PIN_BUTTON_DOWN);
  uint8_t right = digitalRead(PIN_BUTTON_RIGHT);
  uint8_t left = digitalRead(PIN_BUTTON_LEFT);
  
  if (!enter)
  {
    Serial.println(F("ENTER KEY PRESSED"));
    colour_led_0 = colours[B];
    colour_led_1 = colours[B];
    colour_led_2 = colours[B];
    colour_led_3 = colours[B]; 
  }
  else
  {
    colour_led_0 = colours[OFF];
    colour_led_1 = colours[OFF];
    colour_led_2 = colours[OFF];
    colour_led_3 = colours[OFF]; 
  }

  if (!up)
  {
    Serial.println(F("UP KEY PRESSED"));
    colour_led_0 = colours[G];
  }

  if (!down)
  {
    Serial.println(F("DOWN KEY PRESSED"));
    colour_led_1 = colours[G];
  }

  if (!left)
  {
    Serial.println(F("LEFT KEY PRESSED"));
    colour_led_2 = colours[G];
  }

  if (!right)
  {
    Serial.println(F("RIGHT KEY PRESSED"));
    colour_led_3 = colours[G];
  }

  NeoPixelStrip.setPixelColor(0, colour_led_0);
  NeoPixelStrip.setPixelColor(1, colour_led_1);
  NeoPixelStrip.setPixelColor(2, colour_led_3);
  NeoPixelStrip.setPixelColor(3, colour_led_2);
  NeoPixelStrip.show();
  delay(100);
}

void writeEEPROM(uint16_t _ui16_Address, uint8_t _ui8_Data)
{
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);  					// Begin transmission to I2C EEPROM
  Wire.write((int)(_ui16_Address >> 8));           			// MSB Send memory address as two 8-bit bytes
  Wire.write((int)(_ui16_Address & 0xFF));         			// LSB Send memory address as two 8-bit bytes
  Wire.write(_ui8_Data);                          	 		// Send data to be stored
  Wire.endTransmission();                         			// End the transmission
  delay(5);                                       			// Add 5ms delay for EEPROM
}

uint8_t readEEPROM(uint16_t _ui16_Address)
{
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);        		// Begin transmission to I2C EEPROM
  Wire.write((int)(_ui16_Address >> 8));                // MSB Send memory address as two 8-bit bytes
  Wire.write((int)(_ui16_Address & 0xFF));              // LSB Send memory address as two 8-bit bytes
  Wire.endTransmission();                               // End the transmission
  Wire.requestFrom(EEPROM_I2C_ADDRESS, 1);      				// Request one byte of data at current memory address
  uint8_t __ui8_Data =  Wire.read();                    // Read the data and assign to variable
  return __ui8_Data;                                    // Return the data as function output
}

uint8_t updateEEPROM(uint16_t _ui16_Address, uint8_t _ui8_Data)
{
  uint8_t __ui8_Result = 0x00;
  uint8_t __ui8_TempData = readEEPROM(_ui16_Address);
  if (__ui8_TempData != _ui8_Data) { writeEEPROM(_ui16_Address, _ui8_Data); __ui8_Result = 0x01;}
  return __ui8_Result;
}

uint16_t verifyImageDataFromFlashToEEProm(uint8_t _ui8_ImageNumber, const unsigned char * _Bitmap)
{
    uint8_t __ui8_FLASHData, __ui8_EEPROMData;
    uint16_t __ui16_EEPROMAddress = EXT_EEPROM_SYSTEM_ADDRESS_USER_IMAGE + (_ui8_ImageNumber * FILESIZE);
    uint16_t __ui16_Result = 0;
    for (uint16_t __ui16_Loop = 0; __ui16_Loop <= FILESIZE - 1; __ui16_Loop++)
    {
      __ui8_FLASHData = pgm_read_byte(&_Bitmap[__ui16_Loop]);
      __ui8_EEPROMData = readEEPROM(__ui16_EEPROMAddress);
      if (__ui8_FLASHData != __ui8_EEPROMData) {__ui16_Result = __ui16_EEPROMAddress; break;}
      __ui16_EEPROMAddress++;
    }
    ui16_FaultAddress = __ui16_Result;
    return __ui16_Result;
}

void decToHex(uint16_t _ui16_Value, String *_str_ToReturn)															// Convert dec to hex
{
	if      (_ui16_Value <= 15) { *_str_ToReturn = "000";}
	else if (_ui16_Value >= 16   && _ui16_Value <= 255)   { *_str_ToReturn = "00";}
	else if (_ui16_Value >= 256  && _ui16_Value <= 4095)  { *_str_ToReturn = "0";}
	*_str_ToReturn += String(_ui16_Value, HEX);
}
